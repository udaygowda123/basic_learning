*Requires -*
Java 1.14
apache-maven-3.5.4
allure-2.13.5
(Path setting must be Completed for above)
============================================================================================================================================

To Run this (use below on command line)
- mvn clean install -Denv={ENVIRONMENT} [<-Dgroups={GROUPNAME}]

"env" takes one of the following values as input - {STAGING, LOCAL}

"groups" is optional - if not given all the tests will run.
if given, it will run all the tests which are tagged with given name. Name of the Tags can be seen in all the Java files ending with Tests.
ex: SMOKE, REGRESSION, RISK etc

============================================================================================================================================
To generate Allure report on your local machine use below -

allure-2.13.5/bin/./allure serve /home/user/old/zestmoneyriskapiautomation/target/allure-results

Note: change the path of project based on your machine.
