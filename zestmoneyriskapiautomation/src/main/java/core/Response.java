package main.java.core;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

@Getter
@Setter
public class Response {

    private String statusLine;
    private Integer statusCode;
    private String body;
    private HashMap<String,String> headers;

    public String getHeader(String headerKey){
        return headers.get(headerKey);
    }

}
