package main.java.rule_engine.trip_money.endpoints;

import lombok.Getter;

@Getter
public class TripMoneyEndPoint {

    private static TripMoneyEndPoint tripMoneyEndPoint = null;

    private TripMoneyEndPoint() {
    }

    //    This method will provide an instance of TripMoneyEndPoint class
    public static synchronized TripMoneyEndPoint getTripMoneyEndPointInstance() {
        if (tripMoneyEndPoint == null) {
            tripMoneyEndPoint = new TripMoneyEndPoint();
        }
        return tripMoneyEndPoint;
    }

    public String getLinkEndPoint(String accountId) {
        return "/tripmoney/onboarding/accounts/" + accountId + "/link";
    }

    public String getEligibilityEndPoint(String accountId) {
        return "/tripmoney/onboarding/accounts/" + accountId + "/eligibility/EXTENDED";
    }

    public String getTripMoneyStage1EndPoint(String customerId, String merchantId) {
        return "/risk_engine/otrocs/trip_money/stage1/" + customerId + "/merchant/" + merchantId;
    }

    public String getUploadDocEndPoint(String accountId) {
        return "/tripmoney/onboarding/accounts/" + accountId + "/doc/SELFIE/SELFIE";
    }

    public String getAdditionalDetailsEndPoint(String accountId) {
        return "/tripmoney/onboarding/accounts/" + accountId + "/eligibility/CKYC";
    }

}