package main.java.rule_engine.trip_money.actions;

import io.qameta.allure.Step;
import main.java.configurations.Environment;
import main.java.core.Response;
import main.java.ruleExecuterService.endpoints.RuleExecutorServiceEndPoint;
import main.java.rule_engine.trip_money.endpoints.TripMoneyEndPoint;
import main.java.utils.NetworkClientV2;

import java.io.IOException;
import java.util.Map;

public class TripMoneyAction {

    @Step("Linking account")
    public static Response linkAccount(String accountId, String requestBody, Map<String, String> headers) throws IOException {
        return (NetworkClientV2.sendPOST(Environment.tripMoneyHost + TripMoneyEndPoint.getTripMoneyEndPointInstance().getLinkEndPoint(accountId), requestBody, headers));
    }

    @Step("Account eligibility")
    public static Response eligibility(String accountId, String requestBody, Map<String, String> headers) throws IOException {
        return (NetworkClientV2.sendPOST(Environment.tripMoneyHost + TripMoneyEndPoint.getTripMoneyEndPointInstance().getEligibilityEndPoint(accountId), requestBody, headers));
    }

    @Step("Running trip money risk stage 1")
    public static Response tripMoneyStage1(String customerId, String merchantId, Map<String, String> headers) throws IOException {
        return (NetworkClientV2.sendGET(Environment.oldRiskHost + TripMoneyEndPoint.getTripMoneyEndPointInstance().getTripMoneyStage1EndPoint(customerId, merchantId), headers));
    }

    @Step("Uploading document")
    public static String docUpload(String accountId, Map<String, String> headers) throws IOException {
        return (NetworkClientV2.sendPOST1(Environment.tripMoneyHost + TripMoneyEndPoint.getTripMoneyEndPointInstance().getUploadDocEndPoint(accountId), headers));
    }
}
