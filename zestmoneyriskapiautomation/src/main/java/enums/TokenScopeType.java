package main.java.enums;

public enum TokenScopeType {

    INTERNAL_SERVICES("internal_services"),
    USER_JOURNEY("user_journey");

    public final String label;

    private TokenScopeType(String label) {
        this.label = label;
    }
}
