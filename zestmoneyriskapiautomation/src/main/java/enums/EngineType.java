package main.java.enums;

public enum EngineType {

    TRIPMONEYSTAGE1("TripMoneyStage1"),
    TRIPMONEYSTAGE2("TripMoneyStage2");

    public final String label;

    private EngineType(String label) {
        this.label = label;
    }
}
