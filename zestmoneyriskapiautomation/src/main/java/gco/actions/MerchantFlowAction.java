package main.java.gco.actions;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;

import io.qameta.allure.Step;
import main.java.configurations.Environment;
import main.java.core.Response;
import main.java.gco.endpoints.MerchantFlowEndPoint;
import main.java.utils.NetworkClientV2;

public class MerchantFlowAction {

	//GCO flow actions
	@Step("Fetching AvailableEmiPlans")
	public static Response getAvailableEmiPlans(String merchantId, String amount) throws IOException {
		return (NetworkClientV2.sendGET(Environment.dagManagerServiceHost
				+ MerchantFlowEndPoint.getGcoFlowEndPointInstance().getInitialAvailableEmiPlansEndPoint(merchantId, amount)));
	}

	@Step("Check_whitelisting")
	public static Response checkForCustomerWhiteList(String merchantId, String mobilenumber) throws IOException {
		return (NetworkClientV2.sendGET(Environment.dagManagerServiceHost
				+ MerchantFlowEndPoint.getGcoFlowEndPointInstance().checkCustomerIsWhitelistedEndPoint(merchantId, mobilenumber)));
	}

	@Step("Post LoanApplication data")
	public static Response postLoanApplicationDetails(String requestBody, Map<String, String> headers)
			throws IOException {
		return (NetworkClientV2.sendPOST(
				Environment.dagManagerServiceHost + MerchantFlowEndPoint.getGcoFlowEndPointInstance().postLoanApplicationEndPoint(),
				requestBody, headers));
	}

	@Step("Get Available Emi Plans for a product")
	public static Response getAvailableEmiPlansForProduct(String merchantId, int amount, String paymentGateWayId)
			throws IOException {
		return (NetworkClientV2.sendGET(Environment.dagManagerServiceHost + MerchantFlowEndPoint.getGcoFlowEndPointInstance()
		.getAvailableEmiPlansEndPoint(merchantId, amount, paymentGateWayId)));
	}

	@Step("Getting LoanApplications details")
	public static Response getLoanApplicationDetails(String loanApplicationId) throws IOException {
		return (NetworkClientV2.sendGET(Environment.dagManagerServiceHost
				+ MerchantFlowEndPoint.getGcoFlowEndPointInstance().getApplicationFlowLoanApplicationEndPoint(loanApplicationId)));
	}

	@Step("Checking weather the customerdetails present for the applicationn through mobile")
	public static Response postCustomerDetails(String requestBody, Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(Environment.authServiceHost
				+ MerchantFlowEndPoint.getGcoFlowEndPointInstance().postCustomerDetailsEndPoint(),requestBody, headers));
	}

	@Step("Generate OTP")
	public static Response postDetailsToGetOtpId(String requestBody, Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(Environment.authServiceHost
				+ MerchantFlowEndPoint.getGcoFlowEndPointInstance().postDetailsToGetOtpEndPoint(),requestBody,headers));
	}

	@Step("Get_OTP")
	public static Response getOtp(String mobileNumber) throws IOException {
		return (NetworkClientV2.sendGET(Environment.InstoreMerchantHost
				+MerchantFlowEndPoint.getGcoFlowEndPointInstance().getOtpEndPoint(mobileNumber)));
	}

	@Step("Pass_EmailToSignUp")
	public static Response postEmailIdToSignUpWithToken(List<NameValuePair> requestBody, Map<String, String> headers)
			throws IOException {
		return (NetworkClientV2.sendPOST(Environment.authServiceHost
				+ MerchantFlowEndPoint.getGcoFlowEndPointInstance().postEmailToSignUpWithTokenEndPoint(),
				requestBody, headers));
	}

	@Step("ApplicationFlow/LoanApplications-2")
	public static Response postApplicationFlowLoanApplicationFlowAfterCustomerCreation(String customerId,
			String journeyId,String requestBody,Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(
				Environment.dagManagerServiceHost + MerchantFlowEndPoint.getGcoFlowEndPointInstance()
				.postLoanApplicationFlowAfterCustomerCreationEndPoint(journeyId,customerId),requestBody,headers));
	}

	@Step("Posting the Social Details")
	public static Response postPersonalDetails(String requestBody, Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(
				Environment.dagManagerServiceHost + MerchantFlowEndPoint.getGcoFlowEndPointInstance().postPersonalDetailsEndPoint(),
				requestBody, headers));
	}

	@Step("Post Emplyement_Details")
	public static Response postEmploymentDetails(String customerId, String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(Environment.dagManagerServiceHost
				+ MerchantFlowEndPoint.getGcoFlowEndPointInstance().postEmploymentDetailsEndPoint(), requestBody, headers));
	}


	@Step("Accept loan agreement")
	public static Response acceptLoanAgreement(String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(Environment.dagManagerServiceHost
				+ MerchantFlowEndPoint.getGcoFlowEndPointInstance().postLoanAgreementEndPoint(), requestBody, headers));
	}

	@Step("Update loan agreement status")
	public static Response updateLoanAgreementAccepted(String applicationId, String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(Environment.dagManagerServiceHost
				+ MerchantFlowEndPoint.getGcoFlowEndPointInstance().updateAgreementAcceptedStatusEndPoint(applicationId), requestBody, headers));
	}

	@Step("Post Bank details")
	public static Response postBankDetails(String customerId,String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(Environment.dagManagerServiceHost
				+ MerchantFlowEndPoint.getGcoFlowEndPointInstance().postBankDetailsEndPoint(customerId), requestBody, headers));
	}


}
