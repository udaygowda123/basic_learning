package main.java.gco.endpoints;

public class MerchantFlowEndPoint {

	private static MerchantFlowEndPoint gcoFlowEndPoint = null;

	private MerchantFlowEndPoint(){
	}

	//    This method will provide an instance of GcoFlowEndPoint class
	public static synchronized MerchantFlowEndPoint getGcoFlowEndPointInstance(){
		if(gcoFlowEndPoint == null){
			gcoFlowEndPoint = new MerchantFlowEndPoint();
		}
		return gcoFlowEndPoint;
	}

	//GCO merchant flow end points
	public String getInitialAvailableEmiPlansEndPoint(String merchantId, String amount){
		return "/Pricing/V2/Quote/AvailableEmiPlans?merchantId="+merchantId+"&basketAmount="+amount;
	}

	public String checkCustomerIsWhitelistedEndPoint(String merchantId, String mobilenumber){
		return "/whitelisting/api/v1/merchants/"+merchantId +"/phone-numbers/91"+mobilenumber;
	}

	public String postLoanApplicationEndPoint(){
		return "/wallet/api/v2/loanApplication";
	}

	public String getAvailableEmiPlansEndPoint(String merchantId, int amount,String paymentGateWayId){
		return "/Pricing/V2/Quote/AvailableEmiPlans?merchantId="+merchantId+"&basketAmount="+amount+"&PaymentGatewayId="+paymentGateWayId;
	}

	public String getApplicationFlowLoanApplicationEndPoint(String loanApplicationId){
		return "/ApplicationFlow/LoanApplications/"+loanApplicationId;
	}

	public String postCustomerDetailsEndPoint(){
		return "/customer/customerdetails";
	}

	public String postDetailsToGetOtpEndPoint(){
		return "/v2/mobile/otp/";
	}

	public String getOtpEndPoint(String mobilenumber){
		return "/TestFolder/otp.php?contact=91"+mobilenumber;
	}

	public String postEmailToSignUpWithTokenEndPoint(){
		return "/connect/token";
	}

	public String postLoanApplicationFlowAfterCustomerCreationEndPoint(String applicationId, String customerId){
		return "/ApplicationFlow/LoanApplications/"+applicationId+"/Customer/"+customerId;
	}

	public String postPersonalDetailsEndPoint(){
		return "/PersonalDetails/CustomerDetails/v2/SocialDetails";
	}

	public String postEmploymentDetailsEndPoint(){
		return "/PersonalDetails/CustomerDetails/v2/Employment";
	}

	public String postLoanAgreementEndPoint(){
		return "/Repayments/repayments-day";
	}

	public String updateAgreementAcceptedStatusEndPoint(String applicationId){
		return "/ApplicationFlow/LoanApplications/"+applicationId+"/statuses/current";
	}

	public String postBankDetailsEndPoint(String customerId){
		return "/FinancialDetails/BankAccountDetails/"+customerId;
	}

}
