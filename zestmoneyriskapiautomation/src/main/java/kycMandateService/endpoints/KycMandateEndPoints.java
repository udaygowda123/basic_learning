package main.java.kycMandateService.endpoints;

public class KycMandateEndPoints {

	private static KycMandateEndPoints kycMandateEndPoint = null;

	private KycMandateEndPoints(){
	}

	//    This method will provide an instance of GcoFlowEndPoint class
	public static synchronized KycMandateEndPoints getKycMandateFlowEndPointInstance(){
		if(kycMandateEndPoint == null){
			kycMandateEndPoint = new KycMandateEndPoints();
		}
		return kycMandateEndPoint;
	}

	//KYC end points
	public String kycComplienceEndPoint(String customerId) {
		return "/kyc/compliance/api/v1/customers/"+customerId;
	}

	public String uploadSelfiEndPoint(String customerId) {
		return "/documents/api/v1/customers/"+customerId+"/document";
	}

	public String uploadPoiEndPoint(String customerId) {
		return "/documents/api/v1/customers/"+customerId+"/document";
	}

	public String uploadPoaEndPoint(String customerId) {
		return "/documents/api/v1/customers/"+customerId+"/document";
	}

	public String setRelationsDetailsEndPoint() {
		return "/PersonalDetails/CustomerDetails/v2/Relation";
	}

	public String submitKycEndPoint(String customerId) {
		return "/kyc/api/v1/customers/"+customerId;
	}

	public String kycComplienceInitiateEndPoint(String customerId) {
		return "/kyc/compliance/api/v1/customers/"+customerId;
	}

	public String kycIpvAddressEndPoint(String customerId) {
		return "/in-person-verification/schedules/api/v1/customers/"+customerId;
	}

	public String kycIpvScheduleEndPoint(String customerId) {
		return "/in-person-verification/schedules/api/v1/customers/"+customerId;
	}

	public String kycComplience1EndPoint(String customerId) {
		return "/kyc/compliance/api/v1/customers/"+customerId;
	}

	public String uploadAadhar(String customerId) {
		return "/documents/api/v2/customers/"+customerId+"/document";
	}

	public String submitKoshla(String customerId) {
		return "/offline-ekyc/khosla/api/v1/customers/"+customerId+"/submit";
	}
	
	public String uploadMandate(String customerId) {
		return "/documents/api/v1/customers/"+customerId+"/document";
	}
}
