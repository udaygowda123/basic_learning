package main.java.kycMandateService.actions;

import java.io.IOException;
import java.util.Map;
import io.qameta.allure.Step;
import main.java.configurations.Environment;
import main.java.core.Response;
import main.java.kycMandateService.endpoints.KycMandateEndPoints;
import main.java.utils.NetworkClientV2;

public class KycMandateActions {

	//KYC flow

	@Step("Check Kyc complience")
	public static Response postKycComplience(String customerId,String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(Environment.dagManagerServiceHost
				+ KycMandateEndPoints.getKycMandateFlowEndPointInstance().kycComplienceEndPoint(customerId), requestBody, headers));
	}

	@Step("upload the selfie")
	public static Response uploadSelfie(String customerId,String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(Environment.dagManagerServiceHost
				+ KycMandateEndPoints.getKycMandateFlowEndPointInstance().uploadSelfiEndPoint(customerId), requestBody, headers));
	}

	@Step("upload the POI")
	public static Response uploadPoi(String customerId,String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(Environment.dagManagerServiceHost
				+ KycMandateEndPoints.getKycMandateFlowEndPointInstance().uploadPoiEndPoint(customerId), requestBody, headers));
	}

	@Step("upload the POA")
	public static Response uploadPoa(String customerId,String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(Environment.dagManagerServiceHost
				+ KycMandateEndPoints.getKycMandateFlowEndPointInstance().uploadPoaEndPoint(customerId), requestBody, headers));
	}

	@Step("Post relationsl details")
	public static Response postRelationalDetails(String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPUT(Environment.dagManagerServiceHost
				+ KycMandateEndPoints.getKycMandateFlowEndPointInstance().setRelationsDetailsEndPoint(), requestBody, headers));
	}

	@Step("submit kyc details")
	public static Response updateKycDetails(String customerId,String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(Environment.dagManagerServiceHost
				+ KycMandateEndPoints.getKycMandateFlowEndPointInstance().submitKycEndPoint(customerId), requestBody, headers));
	}

	@Step("Initiate the kyc complience")
	public static Response postKycComplienceInitiate(String customerId,String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPATCH(Environment.dagManagerServiceHost
				+ KycMandateEndPoints.getKycMandateFlowEndPointInstance().kycComplienceInitiateEndPoint(customerId), requestBody, headers));
	}

	@Step("Post the KYC IPV address")
	public static Response postKycIpvAddress(String customerId,String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(Environment.dagManagerServiceHost
				+ KycMandateEndPoints.getKycMandateFlowEndPointInstance().kycIpvAddressEndPoint(customerId), requestBody, headers));
	}

	@Step("Post the KYC IPV schedule")
	public static Response postKycIpvSchedule(String customerId,String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPUT(Environment.dagManagerServiceHost
				+ KycMandateEndPoints.getKycMandateFlowEndPointInstance().kycIpvScheduleEndPoint(customerId), requestBody, headers));
	}

	@Step("update the KYC Complience")
	public static Response updateKycComplience(String customerId,String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPATCH(Environment.dagManagerServiceHost
				+ KycMandateEndPoints.getKycMandateFlowEndPointInstance().kycComplience1EndPoint(customerId), requestBody, headers));
	}

	@Step("Upload the Aadhar file")
	public static Response uploadAadhar(String customerId, String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(Environment.dagManagerServiceHost+KycMandateEndPoints.getKycMandateFlowEndPointInstance().uploadAadhar(customerId), requestBody,headers));

	}

	@Step("Submit Koshla")
	public static Response submitKoshla(String customerId, String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(Environment.dagManagerServiceHost+KycMandateEndPoints.getKycMandateFlowEndPointInstance().submitKoshla(customerId),requestBody, headers));
	}
	
	@Step("Upload Mandates")
	public static Response uploadMandate(String customerId,String requestBody,
			Map<String, String> headers) throws IOException {
		return (NetworkClientV2.sendPOST(Environment.dagManagerServiceHost+KycMandateEndPoints.getKycMandateFlowEndPointInstance().uploadAadhar(customerId),requestBody, headers));
	}
}
