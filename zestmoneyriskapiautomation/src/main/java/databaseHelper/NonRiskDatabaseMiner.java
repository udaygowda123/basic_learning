package main.java.databaseHelper;

import main.java.utils.NonRiskConnectionPool;
import main.java.utils.listners.Logger;
import org.json.JSONObject;
import java.sql.*;
import java.util.ArrayList;

public class NonRiskDatabaseMiner {

	public static Object getIdVerificationTransactionTimeStamp(ArrayList customerIds) throws Exception {

		String customers = "";

		JSONObject records = new JSONObject();

		for (int i = 0; i < customerIds.size(); i++) {
			if (i == customerIds.size() - 1) {
				customers = customers + "'" + customerIds.get(i) + "'";
			} else
				customers = customers + "'" + customerIds.get(i) + "',";
		}

		String query = "select zest_customer_id, created_at from id_verification.transaction where zest_customer_id in ("
				+ customers + ") and status = \"SUCCESS\" and transaction_type_id =2";

		Logger.log(query);
		try (Connection connection = NonRiskConnectionPool.getConnection();
				Statement statement = connection.createStatement()) {

			ResultSet resultSet = statement.executeQuery(query);

			while (resultSet.next()) {
				JSONObject record = new JSONObject();
				record.put("created_at", resultSet.getString("created_at"));
				records.put(resultSet.getString("zest_customer_id"), record);
			}

			NonRiskConnectionPool.releaseConnection(connection);
			return records;

		} catch (SQLException ex) {
			Logger.log(ex.getMessage());
		}

		throw new Exception("No result found");
	}

	public static String findCustomerIdByMobileNumber(String mobileNumber) throws Exception {

		// extracting customer_id
		String customerIdByMobileNumberQuery = "SELECT CustomerId FROM comms.Contacts where ContactString = '91"
				+ mobileNumber + "' and Active = 1";
		Logger.log("Query to get customer_id: " + customerIdByMobileNumberQuery);

		String customerId = null;

		try (Connection connection = NonRiskConnectionPool.getConnection();
				Statement statement = connection.createStatement()) {

			ResultSet resultSet = statement.executeQuery(customerIdByMobileNumberQuery);

			if (resultSet.next()) {
				customerId = resultSet.getString("CustomerId");
			}

			NonRiskConnectionPool.releaseConnection(connection);
		} catch (SQLException ex) {
			Logger.log(ex.getMessage());
		}
		return customerId;
	}

	public static String findEmailByCustomerId(String customerid) throws Exception {

		// extracting customer_id
		String emailByMobileNumberQuery = "select * from comms.Contacts where CustomerId='" + customerid
				+ "' and ContactType=2 order by id desc limit 1";
		Logger.log("Query to get customer_id: " + emailByMobileNumberQuery);

		String emailId = null;

		try (Connection connection = NonRiskConnectionPool.getConnection();
				Statement statement = connection.createStatement()) {

			ResultSet resultSet = statement.executeQuery(emailByMobileNumberQuery);

			if (resultSet.next()) {
				emailId = resultSet.getString("ContactString");
			}

			NonRiskConnectionPool.releaseConnection(connection);
		} catch (SQLException ex) {
			Logger.log(ex.getMessage());
		}
		return emailId;
	}

	public static String findPanByCustomerId(String customerId) throws Exception {

		// extracting pan
		String panQuery = "SELECT Pan FROM personal_details.SocialDetails where CustomerId = '" + customerId + "'";
		Logger.log("Query to get pan: " + panQuery);

		String pan = null;

		try (Connection connection = NonRiskConnectionPool.getConnection();
				Statement statement = connection.createStatement()) {

			ResultSet resultSet = statement.executeQuery(panQuery);

			if (resultSet.next()) {
				pan = resultSet.getString("Pan");
			}

			NonRiskConnectionPool.releaseConnection(connection);
		} catch (SQLException ex) {
			Logger.log(ex.getMessage());
		}
		return pan;

	}

	public static void resetTripMoneyUserForRisk(String mobileNumber) throws Exception {

		// extracting customer_id
		String customerIdByMobileNumberQuery = "SELECT CustomerId FROM comms.Contacts where ContactString = '91"
				+ mobileNumber + "' and Active = 1";
		Logger.log("Query to get customer_id: " + customerIdByMobileNumberQuery);

		String customerId = null;

		try (Connection connection = NonRiskConnectionPool.getConnection();
				Statement statement = connection.createStatement()) {

			ResultSet resultSet = statement.executeQuery(customerIdByMobileNumberQuery);

			if (resultSet.next()) {
				customerId = resultSet.getString("CustomerId");
			}

			NonRiskConnectionPool.releaseConnection(connection);
		} catch (SQLException ex) {
			Logger.log(ex.getMessage());
		}

		String mmt_customer_account_mapping = "UPDATE `tripmoney_onboarding`.`mmt_customer_account_mapping` SET `journey_status` = '1', `rejection_code` = NULL, `rejected_at` = NULL where customer_id = '"
				+ customerId + "'";
		String customerStatus = "UPDATE `preapprove`.`CustomerStatus` SET `Status` = NULL WHERE `CustomerId` = '"
				+ customerId + "'";
		String customerPreapproveStates = "UPDATE `preapprove`.`CustomerPreapproveStates` SET `RiskStage1Status` = NULL, `RiskStage1RunDate` = NULL, `Riskstage1ExpiryDate` = NULL, `RejectionCode` = NULL where CustomerId = '"
				+ customerId + "'";

		insertIntoTable(mmt_customer_account_mapping);
		insertIntoTable(customerStatus);
		insertIntoTable(customerPreapproveStates);
	}

	// This method is used to run a insert query, it returns the query execution
	// result as returned by executeUpdate method
	public static int insertIntoTable(String query) {

		int result = -1;

		try (Connection connection = NonRiskConnectionPool.getConnection();
				PreparedStatement insertStatement = connection.prepareStatement(query)) {

			result = insertStatement.executeUpdate();
			NonRiskConnectionPool.releaseConnection(connection);

			Logger.log(result + " " + "Query: " + query);
		} catch (Exception ex) {
			Logger.log(ex.getMessage());
		}
		return result;
	}

	public static String getPaymentGateWayIdForMerchantId(String merchantId) throws Exception {

		String paymentGateWayId = null;
		String query = "SELECT * from pricing.MerchantSettings where MerchantId = '" + merchantId
				+ "' order by Id desc limit 1;";

		Logger.log(query);
		try (Connection connection = NonRiskConnectionPool.getConnection();
				Statement statement = connection.createStatement()) {

			ResultSet resultSet = statement.executeQuery(query);

			while (resultSet.next()) {
				NonRiskConnectionPool.releaseConnection(connection);
				paymentGateWayId = resultSet.getString("PaymentGatewayId");
			}

			NonRiskConnectionPool.releaseConnection(connection);
			return paymentGateWayId;

		} catch (SQLException ex) {
			Logger.log(ex.getMessage());
		}

		throw new Exception("No result found");
	}

	public static String findOtpByMobileNumber(String mobileNumber, String Otp) throws Exception {

		// extracting customer_id
		String otpByMobileNumberQuery = "select * from otp.OtpPasswords  where ToNumber='91" + mobileNumber
				+ "' order By Id desc limit 1;";
		Logger.log("Query to get customer_id: " + otpByMobileNumberQuery);

		String otp = null;

		try (Connection connection = NonRiskConnectionPool.getConnection();
				Statement statement = connection.createStatement()) {

			ResultSet resultSet = statement.executeQuery(otpByMobileNumberQuery);

			if (resultSet.next()) {
				otp = resultSet.getString(Otp);
			}

			NonRiskConnectionPool.releaseConnection(connection);
		} catch (SQLException ex) {
			Logger.log(ex.getMessage());
		}
		return otp;
	}

	public static void updateMerchantInPreApprove(String customerId, String merchantId) {
		String updateMerchantIdQuery = "UPDATE `preapprove`.`CustomerPreapproveStates` SET `MerchantId` = '"
				+ merchantId + "', `SourceMerchantId` = '" + merchantId + "' WHERE (`CustomerId` = '" + customerId
				+ "');";
		insertIntoTable(updateMerchantIdQuery);
	}

}
