package main.java.databaseHelper;

import main.java.utils.RiskConnectionPool;
import main.java.utils.listners.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import java.sql.*;
import java.util.ArrayList;


public class RiskDatabaseMiner {

    public static String getOtrocsResponse(String riskWorkFlowId, String customerId) throws Exception {

        String query = "SELECT * FROM risk.otrocs where CustomerId = '"+customerId+"' and RiskWorkflowId = '"+riskWorkFlowId+"'";

        try (Connection connection = RiskConnectionPool.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(query);

            if (resultSet.next()) {
                RiskConnectionPool.releaseConnection(connection);
                return resultSet.getString("JsonResponse");
            }

            RiskConnectionPool.releaseConnection(connection);

        } catch (SQLException ex) {
            Logger.log(ex.getMessage());
        }

        throw new Exception("No result found");
    }

    public static Object getCustomerJourneyDagByRiskWorkflowId(String riskWorkFlowId) throws Exception {

        String query = "SELECT * FROM risk2.customer_journey_dag  where risk_workflow_id = '"+riskWorkFlowId+"'";

        try (Connection connection = RiskConnectionPool.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(query);

            if (resultSet.next()) {
                RiskConnectionPool.releaseConnection(connection);
                return resultSet.getString("JsonResponse");
            }

            RiskConnectionPool.releaseConnection(connection);

        } catch (SQLException ex) {
            Logger.log(ex.getMessage());
        }

        throw new Exception("No result found");
    }

    public static Object getOtrocsResponses(String[] riskWorkFlowIds) throws Exception {

        String riskWorkFlowIdList = "";

        JSONArray finalResult = new JSONArray();

        for(int i = 0;i<riskWorkFlowIds.length;i++){
            if(i == riskWorkFlowIds.length-1){
                riskWorkFlowIdList = riskWorkFlowIdList + "'"+riskWorkFlowIds[i]+"'";
            }
            else
                riskWorkFlowIdList = riskWorkFlowIdList + "'"+riskWorkFlowIds[i]+"',";
        }

        String query = "SELECT CustomerId, RiskWorkflowId, JsonResponse FROM risk.otrocs where RiskWorkflowId in ("+riskWorkFlowIdList+")";

        Logger.log(query);
        try (Connection connection = RiskConnectionPool.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                JSONObject record = new JSONObject();
                record.put("RiskWorkflowId", resultSet.getString("RiskWorkflowId"));
                record.put("CustomerId", resultSet.getString("CustomerId"));
                record.put("JsonResponse", resultSet.getString("JsonResponse"));
                finalResult.put(record);
            }

            RiskConnectionPool.releaseConnection(connection);
            return finalResult;

        } catch (SQLException ex) {
            Logger.log(ex.getMessage());
        }

        throw new Exception("No result found");
    }

    public static ArrayList<String> getOldRiskWorkflowIdsByEngineTypeForSpecifiedPeriod(String startDate, String endDate, String engineType) throws Exception {

        ArrayList<String> oldRiskWorkFlowIds = new ArrayList<>();

        String query = "SELECT id, CustomerId, RiskWorkflowId, CreatedAt, EngineType FROM risk.ApplicationDecisions where id > (SELECT id FROM risk.ApplicationDecisions where CreatedAt <= '"+startDate+"' order by id desc limit 1) and CreatedAt < '"+endDate+"' and EngineType = '"+engineType+"' order by id;";
        Logger.log(query);
        try (Connection connection = RiskConnectionPool.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                oldRiskWorkFlowIds.add(resultSet.getString("RiskWorkflowId"));
            }

            RiskConnectionPool.releaseConnection(connection);
            Logger.log("No of records returned: "+oldRiskWorkFlowIds.size());
            return oldRiskWorkFlowIds;

        } catch (SQLException ex) {
            Logger.log(ex.getMessage());
        }

        throw new Exception("No result found");
    }

    public static ArrayList<String> getNewRiskWorkflowIdsByEngineTypeForSpecifiedPeriod(String startDate, String endDate, String journeyType) throws Exception {

        ArrayList<String> newRiskWorkFlowIds = new ArrayList<>();

        String query = "select id, risk_workflow_id, customer_id, created_at from risk2.customer_journey_dag where id > (select id from risk2.customer_journey_dag where created_at <= '"+startDate+"' order by id desc limit 1) and created_at < '"+endDate+"' and journey_type = '"+journeyType   +"' order by id";
        Logger.log(query);
        try (Connection connection = RiskConnectionPool.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                newRiskWorkFlowIds.add(resultSet.getString("risk_workflow_id"));
            }

            RiskConnectionPool.releaseConnection(connection);
            Logger.log("No of records returned: "+newRiskWorkFlowIds.size());
            return newRiskWorkFlowIds;

        } catch (SQLException ex) {
            Logger.log(ex.getMessage());
        }

        throw new Exception("No result found");
    }

    public static ArrayList<String> getFinalResultMismatchedRiskWorkflowIdsForSpecifiedPeriod(String startDate, String endDate, String journeyType) throws Exception {

        ArrayList<String> mismachedRiskWorkFlowIds = new ArrayList<>();

        String query = "SELECT\n" +
                "\t*\n" +
                "    FROM\n" +
                "            (SELECT\n" +
                "                     t1.id,\n" +
                "             t1.customer_id,\n" +
                "             t2.RiskWorkflowId ,\n" +
                "             t1.journey_type,\n" +
                "             t1.created_at,\n" +
                "             t1.dag,\n" +
                "             t1.decision AS new_risk_decision,\n" +
                "             t2.decision AS old_risk_decision\n" +
                "                     FROM\n" +
                "                     risk2.customer_journey_dag t1\n" +
                "                     INNER JOIN\n" +
                "                     risk.ApplicationDecisions t2 ON t2.CustomerId = t1.customer_id and t1.risk_workflow_id = t2.riskworkflowid\n" +
                "                     and t1.journey_type = '"+journeyType+"'\n" +
                "                     AND t2.EngineType = t1.journey_type  AND t1.created_at >= '"+startDate+"' AND t1.created_at <= '"+endDate+"'\n" +
                "                     AND t2.CreatedAt >= '"+startDate+"' AND t2.CreatedAt <= '"+endDate+"' where t2.id > (SELECT id FROM risk.ApplicationDecisions where CreatedAt <= '"+startDate+"' order by id desc limit 1)) t11\n" +
                "    WHERE\n" +
                "    t11.new_risk_decision != t11.old_risk_decision and t11.journey_type  = '"+journeyType+"';";

        Logger.log(query);
        try (Connection connection = RiskConnectionPool.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                mismachedRiskWorkFlowIds.add(resultSet.getString("RiskWorkflowId"));
            }

            RiskConnectionPool.releaseConnection(connection);
            Logger.log("No of mismatched records returned: "+mismachedRiskWorkFlowIds.size());
            return mismachedRiskWorkFlowIds;

        } catch (SQLException ex) {
            Logger.log(ex.getMessage());
        }

        throw new Exception("No result found");
    }

    public static Object getCLMismatchedRiskWorkflowIdsForSpecifiedPeriod(String startDate, String endDate) throws Exception {

        JSONArray records = new JSONArray();

        String query = "select R1CL.Id, R2CL.Id, R1CL.CustomerId, R2CL.RiskWorkflowId, R1CL.ApprovedAmount as 'Risk1_ApprovedAmount', R2CL.ApprovedAmount as 'Risk2_ApprovedAmount' FROM risk2.CreditLimit R2CL inner join risk.CreditLimit R1CL\n" +
                " on R2CL.CustomerId = R1CL.CustomerId\n" +
                " and R2CL.RiskWorkflowId = R1CL.RiskWorkflowId\n" +
                " and R1CL.CreatedAt > '"+startDate+"' and R1CL.CreatedAt < '"+endDate+"'\n" +
                " and R2CL.CreatedAt > '"+startDate+"' and R2CL.CreatedAt < '"+endDate+"'\n" +
                " where R1CL.ApprovedAmount != R2CL.ApprovedAmount order by R2CL.Id desc";

        Logger.log(query);
        try (Connection connection = RiskConnectionPool.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                JSONObject record = new JSONObject();
                record.put("RiskWorkflowId", resultSet.getString("RiskWorkflowId"));
                record.put("CustomerId", resultSet.getString("RiskWorkflowId"));
                record.put("Risk1_ApprovedAmount", resultSet.getString("CustomerId"));
                record.put("Risk2_ApprovedAmount", resultSet.getString("Risk2_ApprovedAmount"));
                records.put(record);
            }

            RiskConnectionPool.releaseConnection(connection);
            Logger.log("No of mismatched records returned: "+records.length());
            return records;

        } catch (SQLException ex) {
            Logger.log(ex.getMessage());
        }

        throw new Exception("No result found");
    }

    public static String getCustomerJourneyDag(String customerId, String journeyType) throws Exception {

        String query = "SELECT dag FROM risk2.customer_journey_dag  where customer_id = '"+customerId+"' AND journey_type = '"+journeyType+"'  order by id desc limit 1;";
        Logger.log(query);
        try (Connection connection = RiskConnectionPool.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(query);

            if (resultSet.next()) {
                String dag = resultSet.getString("dag");
                RiskConnectionPool.releaseConnection(connection);
                return dag;
            }

            RiskConnectionPool.releaseConnection(connection);
            Logger.log("No of records returned");

        } catch (SQLException ex) {
            Logger.log(ex.getMessage());
        }

        throw new Exception("No result found");
    }

//    Getting the latest enrichment data from customer table
    public static String getCustomerData(String customerId) throws Exception {

        String query = "SELECT data FROM risk2.customer where customer_id = '"+customerId+"' order by id desc limit 1";
        Logger.log(query);
        try (Connection connection = RiskConnectionPool.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(query);

            if (resultSet.next()) {
                String dag = resultSet.getString("data");
                RiskConnectionPool.releaseConnection(connection);
                return dag;
            }

            RiskConnectionPool.releaseConnection(connection);
            Logger.log("No of records returned");

        } catch (SQLException ex) {
            Logger.log(ex.getMessage());
        }

        throw new Exception("No result found");
    }
}
