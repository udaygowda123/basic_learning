package main.java.databaseHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import main.java.utils.RiskConnectionPool;
import main.java.utils.listners.Logger;

public class DBUtils {

	// Param StringToReplace,Returns String with single quotes
	public static String replaceToString(String str) {
		return "'" + str + "'";
	}

	// Common function to fetch the data from the db with multiple parameters
	public static String fetchDataFromDB(String query, String[] queryParameters) throws Exception {
		String queryParamsArray[] = new String[queryParameters.length];
		for (int i = 0; i < queryParameters.length; i++) {
			queryParamsArray[i] = replaceToString(queryParameters[i]);
		}
		String queryToGetData = new MessageFormat(query).format(queryParamsArray);
		Logger.log(queryToGetData);
		HashMap<String, String> result = getMultipleColumnData(queryToGetData).get(0);
		String value = null;
		if (!result.isEmpty()) {
			Iterator<String> ma = result.keySet().iterator();
			if (ma.hasNext()) {
				value = result.get(ma.next());
			}
		}
		return value;
	}

	// Create query by passing single parameter
	public static String createQuery(String query, String queryParameter) {
		String queryToGetData = MessageFormat.format(query, replaceToString(queryParameter));
		Logger.log(queryToGetData);
		return queryToGetData;

	}

	// Create query by passing single parameter
	public static String createQuery(String query, String[] queryParameters) {
		String queryParamsArray[] = new String[queryParameters.length];
		for (int i = 0; i < queryParameters.length; i++) {
			queryParamsArray[i] = replaceToString(queryParameters[i]);
		}
		String queryToGetData = new MessageFormat(query).format(queryParamsArray);
		Logger.log(queryToGetData);
		return queryToGetData;

	}

	// Common function to fetch the data from the db with single query param
	public static String fetchDataFromDB(String query, String queryParameter) throws Exception {
		String queryToGetData = MessageFormat.format(query, replaceToString(queryParameter));
		Logger.log(queryToGetData);
		String result = getSingleColumnData(queryToGetData);
		return result;
	}

	// Common function to fetch the data from the db with multiple column data in
	// query
	public static ArrayList<HashMap<String, String>> getMultipleColumnData(String query) throws Exception {

		ArrayList<HashMap<String, String>> data = null;

		try (Connection connection = RiskConnectionPool.getConnection();
				Statement statement = connection.createStatement()) {

			ResultSet rs = statement.executeQuery(query);

			ResultSetMetaData metaData = rs.getMetaData();
			int columnCount = metaData.getColumnCount();

			data = new ArrayList<>();
			HashMap<String, String> hm;
			int index = 0;
			while (rs.next()) {
				hm = new HashMap<>();
				for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
					String columname = metaData.getColumnName(columnIndex);
					String ColumnValue = rs.getString(columnIndex);
					hm.put(columname, ColumnValue);
				}
				data.add(index, hm);
				index++;
			}

			RiskConnectionPool.releaseConnection(connection);

		} catch (SQLException ex) {
			Logger.log(ex.getMessage());
		}
		return data;

	}

	// This method returns String for single column
	public static String getSingleColumnData(String query) throws Exception {
		Logger.log(query);
		try (Connection connection = RiskConnectionPool.getConnection();
				Statement statement = connection.createStatement()) {

			ResultSet resultSet = statement.executeQuery(query);

			if (resultSet.next()) {
				int i = resultSet.getMetaData().getColumnCount();
				if (i == 1) {
					String result = resultSet.getString(i);
					RiskConnectionPool.releaseConnection(connection);
					return result;
				} else {
					Logger.log("Use getMultipleColumnData() method");
				}

			}

			RiskConnectionPool.releaseConnection(connection);
			Logger.log("No of records returned");

		} catch (SQLException ex) {
			Logger.log(ex.getMessage());
		}

		throw new Exception("No result found");
	}

}
