package main.java.authService.actions;

import io.qameta.allure.Step;
import main.java.authService.endpoints.AuthServiceEndPoint;
import main.java.configurations.Environment;
import main.java.core.Response;
import main.java.enrichmentService.endpoints.EnrichmentServiceEndPoint;
import main.java.utils.NetworkClientV2;
import org.apache.http.NameValuePair;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class AuthAction {

    @Step("Generating access token")
    public static Response getToken(List<NameValuePair> params, Map<String, String> headers) throws IOException {
        return (NetworkClientV2.sendPOST(Environment.authServiceHost + AuthServiceEndPoint.getAuthServiceEndPointInstance().getToken(), params, headers));
    }
}
