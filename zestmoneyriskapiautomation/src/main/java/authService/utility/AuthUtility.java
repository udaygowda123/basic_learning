package main.java.authService.utility;

import io.qameta.allure.AllureId;
import io.qameta.allure.Step;
import main.java.authService.actions.AuthAction;
import main.java.enums.TokenScopeType;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AuthUtility {

    public static String getAccessToken(TokenScopeType tokenScopeType) throws IOException {

//        Preparing the common HTTP header information
        Map<String, String> tokenRequestHeaders = new HashMap<String, String>();
        tokenRequestHeaders.put("content-type", "application/x-www-form-urlencoded");

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("client_id", "84802e3f-7fde-4eea-914e-b9e70bdfc26f"));
        params.add(new BasicNameValuePair("client_secret", "PoToY%DwGY7Bh{U[t6]M"));
        params.add(new BasicNameValuePair("grant_type", "client_credentials"));
        params.add(new BasicNameValuePair("scope", tokenScopeType.label));

        String access_token = new JSONObject(AuthAction.getToken(params, tokenRequestHeaders).getBody()).getString("access_token");

        return access_token;
    }
}
