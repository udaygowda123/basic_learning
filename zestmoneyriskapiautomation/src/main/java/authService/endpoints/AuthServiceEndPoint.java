package main.java.authService.endpoints;

import lombok.Getter;

@Getter
public class AuthServiceEndPoint {

    private static AuthServiceEndPoint authServiceEndPoint = null;

    private AuthServiceEndPoint(){
    }

//    This method will provide an instance of EnrichmentServiceEndPoint class
    public static synchronized AuthServiceEndPoint getAuthServiceEndPointInstance(){
        if(authServiceEndPoint == null){
            authServiceEndPoint = new AuthServiceEndPoint();
        }
        return authServiceEndPoint;
    }

    private String token = "/connect/token";

}
