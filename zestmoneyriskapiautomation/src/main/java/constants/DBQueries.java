package main.java.constants;

public class DBQueries {

	public final static String GET_DAG_QUERY = "select dag from risk2.customer_journey_dag where risk_workflow_id= {0} order by id desc limit 1";
	public final static String GET_DAG_BY_CUSTOMER_ID_JOURNEY_QUERY = "select dag from risk2.customer_journey_dag where customer_id= {0} and journey_type={1} order by id desc limit 1";
	public final static String GET_RISKWORKFLOWID_QUERY = "select risk_workflow_id from risk2.customer_journey_dag where customer_id= {0} and journey_type={1} order by id desc limit 1";
	public final static String GET_SCORECARD_DATA_QUERY = "select Name, Score, Features from risk.ScoreCard where CustomerId={0} order by id desc";
	public final static String GET_SCORECARD_RISKWORKFLOW_QUERY = "select Name, Score, Features from risk.ScoreCard where RiskWorkflowId={0} order by id desc";
	public final static String GET_DAG_FROM_DAG_REGISTRY = "select dag from risk2.dag_registry where merchant_type={0} and journey_type={1} and flow_type={2} order by id desc limit 1";
}
