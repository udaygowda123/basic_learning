package main.java.constants;

public class RiskConstants {
    // Merchant ID constants
    public static String PINELAB_MERCHANTS = "4369bab6-0f87-4d9b-85df-10718781fd24";
    public static String ZEST_MERCHANT_ID = "a70ce9c4-881a-405d-834a-4a18554fb33a";
    public static String APPLETVS_MERCHANTS = "884c6c79-c17d-402d-b34c-bfb33e8a3637";
    public static String PINESTORE_DP = "dff5c7d6-c5a8-4d28-a069-58aa8d9e14f0";

}
