package main.java.dagManagerService.merchant.actions;

import io.qameta.allure.Step;
import main.java.configurations.Environment;
import main.java.core.Response;
import main.java.dagManagerService.merchant.endpoints.GcoFlowEndPoint;
import main.java.utils.NetworkClientV2;

import java.io.IOException;
import java.util.Map;

public class GcoFLowAction {

    @Step("Running PreAcceptV2")
    public static Response preAcceptV2(String customerId, String journeyId, Map<String, String> headers) throws IOException {
        return (NetworkClientV2.sendGET(Environment.dagManagerServiceHost + GcoFlowEndPoint.getGcoFlowEndPointInstance().getPreAcceptV2EndPoint(customerId, journeyId), headers));
    }

    @Step("Running PostAcceptV2")
    public static Response postAcceptV2(String customerId, String journeyId, Map<String, String> headers) throws IOException {
        return (NetworkClientV2.sendGET(Environment.dagManagerServiceHost + GcoFlowEndPoint.getGcoFlowEndPointInstance().getPostAcceptV2EndPoint(customerId, journeyId), headers));
    }
}
