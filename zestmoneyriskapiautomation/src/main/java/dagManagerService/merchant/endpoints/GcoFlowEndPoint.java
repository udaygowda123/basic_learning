package main.java.dagManagerService.merchant.endpoints;

import lombok.Getter;

@Getter
public class GcoFlowEndPoint {

    private static GcoFlowEndPoint gcoFlowEndPoint = null;

    private GcoFlowEndPoint(){
    }

//    This method will provide an instance of GcoFlowEndPoint class
    public static synchronized GcoFlowEndPoint getGcoFlowEndPointInstance(){
        if(gcoFlowEndPoint == null){
            gcoFlowEndPoint = new GcoFlowEndPoint();
        }
        return gcoFlowEndPoint;
    }

    public String getPreAcceptV2EndPoint(String customerId, String journeyId){
        return "/dag-manager/api/v1/otrocs/preaccept/v2/"+customerId+"/"+journeyId;
    }

    public String getPostAcceptV2EndPoint(String customerId, String journeyId){
        return "/dag-manager/api/v1/otrocs/postaccept/v2/"+customerId+"/"+journeyId;
    }

}
