package main.java.dagManagerService.cl.endpoints;

import lombok.Getter;

@Getter
public class ClFlowEndPoint {

    private static ClFlowEndPoint clFlowEndPoint = null;

    private ClFlowEndPoint(){
    }

//    This method will provide an instance of ClFlowEndPoint class
    public static synchronized ClFlowEndPoint getClFlowEndPointInstance(){
        if(clFlowEndPoint == null){
            clFlowEndPoint = new ClFlowEndPoint();
        }
        return clFlowEndPoint;
    }

    public String getPreApproveV2EndPoint(String customerId){
        return "/dag-manager/api/v1/otrocs/preapprove/v2/"+customerId;
    }

}
