package main.java.dagManagerService.cl.actions;

import io.qameta.allure.Step;
import main.java.configurations.Environment;
import main.java.core.Response;
import main.java.dagManagerService.cl.endpoints.ClFlowEndPoint;
import main.java.utils.NetworkClientV2;

import java.io.IOException;
import java.util.Map;

public class ClFlowAction {

    @Step("Running PreApproveV2Stage")
    public static Response preapproveV2stage(String customerId, Map<String, String> headers) throws IOException {
        return (NetworkClientV2.sendGET(Environment.dagManagerServiceHost + ClFlowEndPoint.getClFlowEndPointInstance().getPreApproveV2EndPoint(customerId), headers));
    }
}
