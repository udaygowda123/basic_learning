package main.java.configurations;

import main.java.enums.EnvironmentType;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class Environment {

    public static EnvironmentType environment;

    public static String dagManagerServiceHost;
    public static String enrichmentServiceHost;
    public static String ruleExecutorServiceHost;
    public static String authServiceHost;
    public static String tripMoneyHost;
    public static String oldRiskHost;
    public static String InstoreMerchantHost;

    public static void prepareEnvironment(EnvironmentType environmentType) throws Exception {

        environment = environmentType;

        String path;

        switch (environmentType) {
            case STAGING:
//              Extracting the path of configurations package
                path = Environment.class.getClassLoader().getResource("configurations").getPath();
                path = path + File.separator + "StagingConfiguration.properties";
                break;

            case LOCAL:
//              Extracting the path of configurations package
                path = Environment.class.getClassLoader().getResource("configurations").getPath();
                path = path + File.separator + "Local.properties";
                break;

            case TEST:
//              Extracting the path of configurations package
                path = Environment.class.getClassLoader().getResource("configurations").getPath();
                path = path + File.separator + "TestConfiguration.properties";
                break;

            case PROD:
//              Extracting the path of configurations package
                path = Environment.class.getClassLoader().getResource("configurations").getPath();
                path = path + File.separator + "ProdConfiguration.properties";
                break;

            default:
                throw new Exception("Undefined environment");

        }

        Properties environmentProperty = new Properties();
        InputStream input = new FileInputStream(path);
        environmentProperty.load(input);

        dagManagerServiceHost = environmentProperty.getProperty("dagManagerServiceHost");
        enrichmentServiceHost = environmentProperty.getProperty("enrichmentServiceHost");
        ruleExecutorServiceHost = environmentProperty.getProperty("ruleExecutorServiceHost");
        authServiceHost = environmentProperty.getProperty("authServiceHost");
        tripMoneyHost = environmentProperty.getProperty("tripMoneyHost");
        oldRiskHost = environmentProperty.getProperty("oldRiskHost");
        InstoreMerchantHost=environmentProperty.getProperty("InstoreMerchantHost");

        input.close();
    }

}
