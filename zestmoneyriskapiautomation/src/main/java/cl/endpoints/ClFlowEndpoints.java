package main.java.cl.endpoints;

public class ClFlowEndpoints {
	private static ClFlowEndpoints clflowendpoints=null;

	public ClFlowEndpoints() {

	}

	//  This method will provide an instance of GcoFlowEndPoint class
	public static synchronized ClFlowEndpoints getClFlowEndpoints(){
		if(clflowendpoints == null){
			clflowendpoints = new ClFlowEndpoints();
		}
		return clflowendpoints;
	}

	//Generate OTP
	public String getOtpEndPoint(){
		return "/v2/mobile/otp/";
	}
	
	//Create User
	public String getCreateUserEndPoint() {
		return "/v2/users";
	}
}
