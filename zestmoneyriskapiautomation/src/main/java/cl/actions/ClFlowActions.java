package main.java.cl.actions;

import java.io.IOException;
import java.util.Map;
import io.qameta.allure.Step;
import main.java.cl.endpoints.ClFlowEndpoints;
import main.java.configurations.Environment;
import main.java.core.Response;
import main.java.utils.NetworkClientV2;

public class ClFlowActions {

	// Generate Otp Action
	@Step("Generate Otp")
	public static Response generateOtp(String requestBody, Map<String, String> requestHeaders) throws IOException {
		return (NetworkClientV2.sendPOST(
				Environment.authServiceHost + ClFlowEndpoints.getClFlowEndpoints().getOtpEndPoint(), requestBody,
				requestHeaders));
	}

	// Generate Otp Action
	@Step("Create User")
	public static Response createUser(String requestBody, Map<String, String> requestHeaders) throws IOException {
		return (NetworkClientV2.sendPOST(
				Environment.authServiceHost + ClFlowEndpoints.getClFlowEndpoints().getCreateUserEndPoint(), requestBody,
				requestHeaders));
	}
}
