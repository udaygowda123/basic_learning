package main.java.utils;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class CommonUtility {

//    This method generates random Pan Numbers
    public static String randomPAN() {
        String firstpart = randomchar(3);
        String secondpart = randomchar(1);
        String thirdpart = randomdigit(4);
        String fourthPart = randomchar(1);
        return firstpart + "P" + secondpart + thirdpart + fourthPart;
    }

    public static String randomchar(int numberofchars) {
        final String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuffer sb = null;
        final int N = alphabet.length();
        Random r = new Random();
        sb = new StringBuffer();
        for (int i = 0; i < numberofchars; i++) {
            sb = sb.append(alphabet.charAt(r.nextInt(N)));
        }
        return sb.toString();
    }

    private static String randomdigit(int numberofchars) {
        final String alphabet = "1234567890";
        StringBuffer sb = null;
        final int N = alphabet.length();
        Random r = new Random();
        sb = new StringBuffer();
        for (int i = 0; i < numberofchars; i++) {
            sb = sb.append(alphabet.charAt(r.nextInt(N)));
        }
        return sb.toString();
    }

//    To generate the first digit of mobile number from a given set of digits
    public static String randomdigitPhone(int numberofchars) {
        final String alphabet = "56789";
        StringBuffer sb = null;
        final int N = alphabet.length();
        Random r = new Random();
        sb = new StringBuffer();
        for (int i = 0; i < numberofchars; i++) {
            sb = sb.append(alphabet.charAt(r.nextInt(N)));
        }
        return sb.toString();
    }

//    This is to generate random mobileNumber
    public static String randomMobileNumber() {
        String firstPart = randomdigitPhone(1);
        String secondPart = randomdigit(9);
        return firstPart + secondPart;
    }
    public static String findDOBByAge(int age){

        Calendar calendar = Calendar.getInstance();
        // Subtract allowed age age in terms of years
        calendar.add(Calendar.YEAR, -age);

        Date currentTime = calendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        return sdf.format(currentTime);
    }
}
