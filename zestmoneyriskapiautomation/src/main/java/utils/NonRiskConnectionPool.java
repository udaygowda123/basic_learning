package main.java.utils;

import lombok.Getter;
import main.java.configurations.Environment;
import main.java.utils.listners.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Properties;

@Getter
public class NonRiskConnectionPool {

    private static NonRiskConnectionPool nonRiskConnectionPool;

    private String dbUser;
    private String dbKey;
    private String dbName;
    private String dbHost;
    private int createdConnections = 0;
    private int initialSize = 0;
    private LinkedList<Connection> non_risk_connection_pool = new LinkedList<>();

    private NonRiskConnectionPool(){}

    /** Preparing the connection pool for selected environment*/
    public static void prepareConnectionPool() throws Exception {

        if(nonRiskConnectionPool != null){
            Logger.log("Warning: A pool already exists");
            return;
        }

        String path;

        switch (Environment.environment){
            case STAGING:
                path = NonRiskConnectionPool.class.getClassLoader().getResource("configurations").getPath();
                path = path + File.separator + "StagingMYSQL.properties";
                break;

            case TEST:
                path = NonRiskConnectionPool.class.getClassLoader().getResource("configurations").getPath();
                path = path + File.separator + "TestMYSQL.properties";
                break;

            case LOCAL:
                path = NonRiskConnectionPool.class.getClassLoader().getResource("configurations").getPath();
                path = path + File.separator + "LocalMYSQL.properties";
                break;
                
            case PROD:
                path = NonRiskConnectionPool.class.getClassLoader().getResource("configurations").getPath();
                path = path + File.separator + "ProdMYSQL.properties";
                break;

            default:
                throw new Exception("Undefined environment type");
        }

        nonRiskConnectionPool = new NonRiskConnectionPool();

        Properties properties = new Properties();
        properties.load(new FileInputStream(path));
        
        nonRiskConnectionPool.dbHost = properties.getProperty("NonRiskMySQLHost");

        nonRiskConnectionPool.dbName = properties.getProperty("NonRiskMySQLDbName");
        nonRiskConnectionPool.dbUser = properties.getProperty("NonRiskMySQLUser");
        nonRiskConnectionPool.dbKey = properties.getProperty("NonRiskMySQLPassword");
        nonRiskConnectionPool.initialSize = Integer.parseInt(properties.getProperty("NonRiskInitialPoolSize"));

        for(int i=0; i<nonRiskConnectionPool.initialSize; i++){
            nonRiskConnectionPool.non_risk_connection_pool.add(nonRiskConnectionPool.createConnection());
        }
    }

    /** This method creates a new connection*/
    private Connection createConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        nonRiskConnectionPool.createdConnections++;
        return DriverManager.getConnection(dbHost, dbUser, dbKey);
    }

    /** This method adds a connection to the pool, if pool was empty, else keep poping a connection until an open connection was found and then returns it.*/
    public static synchronized Connection getConnection() throws Exception{

        if (nonRiskConnectionPool.getNon_risk_connection_pool().size() == 0)
                nonRiskConnectionPool.getNon_risk_connection_pool().add(nonRiskConnectionPool.createConnection());

        Connection risk_connection = nonRiskConnectionPool.getNon_risk_connection_pool().pop();

        while (risk_connection.isClosed()){
            if(nonRiskConnectionPool.getNon_risk_connection_pool().size() == 0){
                nonRiskConnectionPool.getNon_risk_connection_pool().add(nonRiskConnectionPool.createConnection());
            }
            risk_connection = nonRiskConnectionPool.getNon_risk_connection_pool().pop();
        }

        return risk_connection;
    }

    /** This method will add the supplied connection to the pool, if it is not closed and if pool is not already full.*/
    public static synchronized void releaseConnection(Connection connection) throws SQLException {

        if(nonRiskConnectionPool.getNon_risk_connection_pool().size() < nonRiskConnectionPool.getInitialSize() && !connection.isClosed()){
            nonRiskConnectionPool.getNon_risk_connection_pool().add(connection);
        } else
            connection.close();
    }

    /** This method will close all the connections present into the pool*/
    public static void shutDownPool() throws SQLException {
        for(int i = 0; i < nonRiskConnectionPool.getNon_risk_connection_pool().size(); i++ )
            nonRiskConnectionPool.getNon_risk_connection_pool().pop().close();
    }
}