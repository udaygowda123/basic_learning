package main.java.utils;

import lombok.Getter;
import main.java.configurations.Environment;
import main.java.utils.listners.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Properties;

@Getter
public class RiskConnectionPool {

    private static RiskConnectionPool riskConnectionPool;

    private String dbUser;
    private String dbKey;
    private String dbName;
    private String dbHost;
    private int activeConnectionsCount = 0;
    private int initialConnectionPoolSize = 0;
    private LinkedList<Connection> risk_connection_pool = new LinkedList<>();

    private RiskConnectionPool(){}

    /** Preparing the connection pool for selected environment*/
    public static void prepareConnectionPool() throws Exception {

        if(riskConnectionPool != null){
            Logger.log("Warning: A pool already exists");
            return;
        }

        String path;

        switch (Environment.environment){
            case STAGING:
                path = RiskConnectionPool.class.getClassLoader().getResource("configurations").getPath();
                path = path + File.separator + "StagingMYSQL.properties";
                break;

            case TEST:
                path = RiskConnectionPool.class.getClassLoader().getResource("configurations").getPath();
                path = path + File.separator + "TestMYSQL.properties";
                break;

            case LOCAL:
                path = RiskConnectionPool.class.getClassLoader().getResource("configurations").getPath();
                path = path + File.separator + "LocalMYSQL.properties";
                break;
                
            case PROD:
                path = RiskConnectionPool.class.getClassLoader().getResource("configurations").getPath();
                path = path + File.separator + "ProdMYSQL.properties";
                break;

            default:
                throw new Exception("Undefined environment type");
        }

        riskConnectionPool = new RiskConnectionPool();

        Properties properties = new Properties();
        properties.load(new FileInputStream(path));
        
        riskConnectionPool.dbHost = properties.getProperty("riskMySQLHost");

        riskConnectionPool.dbName = properties.getProperty("riskMySQLDbName");
        riskConnectionPool.dbUser = properties.getProperty("riskMySQLUser");
        riskConnectionPool.dbKey = properties.getProperty("riskMySQLPassword");
        riskConnectionPool.initialConnectionPoolSize = Integer.parseInt(properties.getProperty("riskInitialPoolSize"));

        for(int i=0; i<riskConnectionPool.initialConnectionPoolSize; i++){
            riskConnectionPool.risk_connection_pool.add(riskConnectionPool.createConnection());
        }
    }

    /** This method creates a new connection*/
    private Connection createConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        riskConnectionPool.activeConnectionsCount++;
        return DriverManager.getConnection(dbHost, dbUser, dbKey);
    }

    /** This method adds a connection to the pool, if pool was empty, else keep poping a connection until an open connection was found and then returns it.*/
    public static synchronized Connection getConnection() throws Exception{

        if (riskConnectionPool.getRisk_connection_pool().size() == 0)
                riskConnectionPool.getRisk_connection_pool().add(riskConnectionPool.createConnection());

        Connection risk_connection = riskConnectionPool.getRisk_connection_pool().pop();

        while (risk_connection.isClosed()){
            -- riskConnectionPool.activeConnectionsCount;
            if(riskConnectionPool.getRisk_connection_pool().size() == 0){
                riskConnectionPool.getRisk_connection_pool().add(riskConnectionPool.createConnection());
            }
            risk_connection = riskConnectionPool.getRisk_connection_pool().pop();
        }

        return risk_connection;
    }

    /** This method will add the supplied connection to the pool, if it is not closed and if pool is not already full.*/
    public static synchronized void releaseConnection(Connection connection) throws SQLException {

        if(riskConnectionPool.getRisk_connection_pool().size() < riskConnectionPool.getInitialConnectionPoolSize() && !connection.isClosed()){
            riskConnectionPool.getRisk_connection_pool().add(connection);
        } else {
            connection.close();
            -- riskConnectionPool.activeConnectionsCount;
        }
    }

    /** This method will close all the connections present into the pool*/
    public static void shutDownPool() throws SQLException {
        for(int i = 0; i < riskConnectionPool.getRisk_connection_pool().size(); i++ ) {
            riskConnectionPool.getRisk_connection_pool().pop().close();
            -- riskConnectionPool.activeConnectionsCount;
        }
    }
}