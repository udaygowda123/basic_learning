package main.java.ruleExecuterService.endpoints;

import lombok.Getter;

@Getter
public class RuleExecutorServiceEndPoint {

    private static RuleExecutorServiceEndPoint ruleExecutorServiceEndPoint = null;

    private RuleExecutorServiceEndPoint(){
    }

//    This method will provide an instance of RuleExecutorServiceEndPoint class
    public static synchronized RuleExecutorServiceEndPoint getRuleExecutorServiceEndPointInstance(){
        if(ruleExecutorServiceEndPoint == null){
            ruleExecutorServiceEndPoint = new RuleExecutorServiceEndPoint();
        }
        return ruleExecutorServiceEndPoint;
    }

    public String getRuleExecutorEndPoint(String journeyId){
        return "/rule-executor/api/v1/journey/"+journeyId+"/rules";
    }

}
