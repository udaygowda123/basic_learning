package main.java.ruleExecuterService.actions;

import io.qameta.allure.Step;
import main.java.configurations.Environment;
import main.java.core.Response;
import main.java.enrichmentService.endpoints.EnrichmentServiceEndPoint;
import main.java.ruleExecuterService.endpoints.RuleExecutorServiceEndPoint;
import main.java.utils.NetworkClientV2;

import java.io.IOException;
import java.util.Map;

public class RuleExecutorAction {

    @Step("Running Rule Engine")
    public static Response runRuleEngine(String journeyId, String requestBody, Map<String, String> headers) throws IOException {
        return (NetworkClientV2.sendPOST(Environment.ruleExecutorServiceHost + RuleExecutorServiceEndPoint.getRuleExecutorServiceEndPointInstance().getRuleExecutorEndPoint(journeyId), requestBody, headers));
    }
}
