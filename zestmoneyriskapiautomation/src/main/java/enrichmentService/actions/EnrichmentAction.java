package main.java.enrichmentService.actions;

import io.qameta.allure.Step;
import main.java.configurations.Environment;
import main.java.core.Response;
import main.java.enrichmentService.endpoints.EnrichmentServiceEndPoint;
import main.java.utils.NetworkClientV2;

import java.io.IOException;
import java.util.Map;

public class EnrichmentAction {

    @Step("Running Enrichment")
    public static Response enrich(String requestBody, Map<String, String> headers) throws IOException {
        return (NetworkClientV2.sendPOST(Environment.enrichmentServiceHost + EnrichmentServiceEndPoint.getEnrichmentServiceEndPointInstance().getEnrichmentEndPoint(), requestBody, headers));
    }
}
