package main.java.enrichmentService.dtos;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "journey_id",
        "journey_type",
        "data_version",
        "merchant_id",
        "customer_id",
        "next_enrichment_set"
})

@Getter
@Setter
public class EnrichmentRequest implements Serializable
{

    @JsonProperty("journey_id")
    private String journeyId;
    @JsonProperty("journey_type")
    private String journeyType;
    @JsonProperty("data_version")
    private String dataVersion;
    @JsonProperty("merchant_id")
    private String merchantId;
    @JsonProperty("customer_id")
    private String customerId;
    @JsonProperty("next_enrichment_set")
    private List<String> nextEnrichmentSet = null;
}