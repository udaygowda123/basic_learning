package main.java.enrichmentService.endpoints;

import lombok.Getter;

@Getter
public class EnrichmentServiceEndPoint {

    private static EnrichmentServiceEndPoint enrichmentServiceEndPoint = null;

    private EnrichmentServiceEndPoint(){
    }

//    This method will provide an instance of EnrichmentServiceEndPoint class
    public static synchronized EnrichmentServiceEndPoint getEnrichmentServiceEndPointInstance(){
        if(enrichmentServiceEndPoint == null){
            enrichmentServiceEndPoint = new EnrichmentServiceEndPoint();
        }
        return enrichmentServiceEndPoint;
    }

    private String enrichmentEndPoint = "/enrichmentService/v1/run_enrichment";

}
