package test.java;

import io.qameta.allure.*;
import main.java.databaseHelper.CibilDatabaseMiner;
import main.java.databaseHelper.NonRiskDatabaseMiner;
import main.java.databaseHelper.RiskDatabaseMiner;
import main.java.interfaces.Group;
import main.java.utils.listners.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import test.resources.dataproviders.EngineTypeDataprovider;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

public class EngineTypeMissingDataTests extends Commons {

    @Test(enabled = false, description = "Verify if any RiskWorkFlowId from Risk 1 is missing in Risk 2 for a specific engine type", groups = {Group.RISK}, dataProvider="ENGINE_TYPE", dataProviderClass = EngineTypeDataprovider.class)
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify if any RiskWorkFlowId from Risk 1 is missing in Risk 2 for a specific engine type")
    public void verifyNoOldRiskWorkflowIdsMissingInNewRiskWorkflowIdsList(String testName, String engineType) throws Exception {

        Date currentTime = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String endDate = sdf.format(currentTime);
        String startDate = "2020-09-01 04:30:00";
//        endDate = "2020-08-24 18:30:00";

        ArrayList<String> oldRiskWorkFlowIds = RiskDatabaseMiner.getOldRiskWorkflowIdsByEngineTypeForSpecifiedPeriod(startDate,endDate,engineType);
        ArrayList<String> newRiskWorkFlowIds = RiskDatabaseMiner.getNewRiskWorkflowIdsByEngineTypeForSpecifiedPeriod(startDate,endDate,engineType);

        ArrayList<String> missingRiskWorkFlowIds = new ArrayList<>();

        for (int i =0; i<oldRiskWorkFlowIds.size();i++){
            if (!newRiskWorkFlowIds.contains(oldRiskWorkFlowIds.get(i))){
                missingRiskWorkFlowIds.add(oldRiskWorkFlowIds.get(i));
            }
        }

        if(missingRiskWorkFlowIds.size()!=0){
            String list = "";
            for (int i =0; i<missingRiskWorkFlowIds.size();i++){
               list = list + missingRiskWorkFlowIds.get(i) + "\n";
            }
            Logger.log("EngineType: "+ engineType+ " Missing RiskWorkflowIds count: "+missingRiskWorkFlowIds.size()+" List: "+list);
            Assert.fail("Missing RiskWorkflowIds count: "+missingRiskWorkFlowIds.size());
        }
    }

    @Test(enabled = false, description = "Extracting final result mismatched riskWorkflowIds for a specific engine type", groups = {Group.RISK1}, dataProvider="ENGINE_TYPE", dataProviderClass = EngineTypeDataprovider.class)
    @Severity(SeverityLevel.BLOCKER)
    @Description("Extracting final result mismatched riskWorkflowIds for a specific engine type")
    public void verifyFinalResultMismatchedRiskWorkflowIdsList(String testName, String engineType) throws Exception {

        Date currentTime = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String endDate = sdf.format(currentTime);
        String startDate = "2020-08-24 18:30:00";

        ArrayList<String> mismatchedRiskWorkflowIds = RiskDatabaseMiner.getFinalResultMismatchedRiskWorkflowIdsForSpecifiedPeriod(startDate,endDate,engineType);

        if(mismatchedRiskWorkflowIds.size()!=0){
            String list = "";
            for (int i =0; i<mismatchedRiskWorkflowIds.size();i++){
                list = list + mismatchedRiskWorkflowIds.get(i) + "\n";
            }
            Assert.fail("Final result mismatched RiskWorkflowIds of flow "+engineType+ " for the period "+startDate+" - "+endDate+" count: "+mismatchedRiskWorkflowIds.size()+" \nList: "+list);
        }
    }

    @Test(enabled = false, description = "Extracting credit limit mismatched riskWorkflowIds for a specific engine type", groups = {Group.RISK2})
    @Severity(SeverityLevel.BLOCKER)
    @Description("Extracting credit limit mismatched riskWorkflowIds for a specific engine type")
    public void verifyCreditMismatchedRiskWorkflowIdsList() throws Exception {

        Date currentTime = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String endDate = sdf.format(currentTime);
        String startDate = "2020-08-24 18:30:00";

        JSONArray mismatchedRiskWorkflowIds = (JSONArray) RiskDatabaseMiner.getCLMismatchedRiskWorkflowIdsForSpecifiedPeriod(startDate, endDate);

        System.out.println(mismatchedRiskWorkflowIds);

//        if(mismatchedRiskWorkflowIds.size()!=0){
//            String list = "";
//            for (int i =0; i<mismatchedRiskWorkflowIds.size();i++){
//                list = list + mismatchedRiskWorkflowIds.get(i) + "\n";
//            }
//            Assert.fail("Final result mismatched RiskWorkflowIds of flow "+engineType+ " for the period "+startDate+" - "+endDate+" count: "+mismatchedRiskWorkflowIds.size()+" \nList: "+list);
//        }
    }



    @Test(enabled = false, description = "Extracting credit limit mismatched riskWorkflowIds for a specific engine type", groups = "K1")
    @Severity(SeverityLevel.BLOCKER)
    @Description("Extracting credit limit mismatched riskWorkflowIds for a specific engine type")
    public void verifyCreditMismatchedRiskWorkflowIdsList1() throws Exception {


        String riskWorkFlowIds[] = {"466b0727-22d0-488a-9992-0aced119b34c",
                "5d96e1ea-73d9-4198-ac3a-9d43ca2c9972",
                "fb04585b-a2d4-4aa8-bf45-73a43eda6a0e",
                "b4efc8de-3918-4d78-869b-9e3c0039487d",
                "d26f8fee-b7be-424b-8596-75532cf9eea7",
                "a0b1ca58-1c24-4abe-a3a1-03f2baaebded",
                "0b3fce80-dd13-460e-9d1c-efa91f244a64",
                "688e1f59-d410-47df-8644-abcc506b82d2",
                "86b3b0a8-edb0-48cc-94c6-667c241afbc6",
                "fe9aa2f5-ee7d-46d3-a548-9f52f6588103",
                "d3811774-1b08-4c2e-976c-e969703327d3",
                "833ae3f5-6764-46bf-ab91-79bb9b3ca25b",
                "e88cf8c1-dfe3-442c-8b5b-13b83ff101bd",
                "649d1563-ac85-454c-a292-b7569affb7ae",
                "ef68ff72-f0d5-4853-ba55-7bab0ee0bcfa",
                "96cb5f92-f3e2-4a46-ab6a-83f25201d292",
                "d215f8cb-3427-4e57-ad79-3a6cda78172c",
                "e4757faa-9594-4740-921a-91b6fa635c1f"};


        JSONArray ortrocsResponseList = (JSONArray) RiskDatabaseMiner.getOtrocsResponses(riskWorkFlowIds);

        ArrayList<String> customers = new ArrayList<>();

        for (int i = 0; i< ortrocsResponseList.length();i++){
            customers.add(ortrocsResponseList.getJSONObject(i).getString("CustomerId"));
        }

        JSONObject verificationRecord = (JSONObject) NonRiskDatabaseMiner.getIdVerificationTransactionTimeStamp(customers);

        for (int i = 0; i< ortrocsResponseList.length();i++){

            String jsonOrtrocsResponse = ortrocsResponseList.getJSONObject(i).getString("JsonResponse");
            JSONObject tempJsonOrtrocsResponse = new JSONObject(jsonOrtrocsResponse);
            String timeStamp = tempJsonOrtrocsResponse.getJSONObject("PANNameCheck").getString("end_time");

            System.out.println(ortrocsResponseList.getJSONObject(i).getString("RiskWorkflowId")+ "  "+ortrocsResponseList.getJSONObject(i).getString("CustomerId")+" "+timeStamp);
//            String tempCustomerId = ortrocsResponseList.getJSONObject(i).getString("CustomerId");
//            ortrocsResponseList.getJSONObject(i).put("Verification_Completion_timestamp", verificationRecord.getJSONObject(tempCustomerId).getString("created_at"));
//

//            JSONObject tempJsonOrtrocsResponse = new JSONObject(jsonOrtrocsResponse);
//            String timeStamp = tempJsonOrtrocsResponse.getJSONObject("PANNameCheck").getString("end_time");
//            ortrocsResponseList.getJSONObject(i).remove("JsonResponse");
//            ortrocsResponseList.getJSONObject(i).put("ortrocsPanNameCheckCompletionTimeStamp",timeStamp);
        }

//        Logger.log(ortrocsResponseList.toString());

    }

}
