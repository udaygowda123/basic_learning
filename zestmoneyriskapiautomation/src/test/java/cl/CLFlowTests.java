package test.java.cl;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import main.java.authService.utility.AuthUtility;
import main.java.cl.actions.ClFlowActions;
import main.java.constants.DBQueries;
import main.java.constants.RiskConstants;
import main.java.dagManagerService.cl.actions.ClFlowAction;
import main.java.databaseHelper.CibilDatabaseMiner;
import main.java.databaseHelper.DBUtils;
import main.java.databaseHelper.NonRiskDatabaseMiner;
import main.java.enums.TokenScopeType;
import main.java.gco.actions.MerchantFlowAction;
import main.java.interfaces.Group;
import main.java.utils.CommonFunctions;
import main.java.utils.CommonUtility;
import main.java.utils.listners.Logger;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import test.java.Commons;
import test.resources.dataproviders.CLFlowDataProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class CLFlowTests extends Commons {

    @Test(description = "Verifies the CL end to end flow with sanity test cases", groups = {Group.CL_FLOW})
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verifies the CL end to end flow with sanity test cases like all scorecards score and features are stored in scorecard table")
    public void verifyCLFlow() throws Exception {

        String mobileNumber = CommonUtility.randomMobileNumber();
        String email = mobileNumber + "@yopmail.com";
        String customerId;
        String pan = CommonUtility.randomPAN();
        String BankNumber = CommonUtility.randomdigitPhone(20);
        String merchantId = RiskConstants.ZEST_MERCHANT_ID;

        String access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

        // Preparing the common HTTP header information
        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("Authorization", "Bearer " + access_token);

        // Preparing the common HTTP header information
        Map<String, String> userjourneyrequestHeaders = new HashMap<>();
        userjourneyrequestHeaders.put("Content-Type", "application/json");

        // Create customer
        JSONObject requestPayload = new JSONObject("{\"emailAddress\": \"\",\"MobileNumber\": \"\"}}");
        requestPayload.put("emailAddress", email);
        requestPayload.put("MobileNumber", "91" + mobileNumber);
        JSONObject createUserResponseJSON = new JSONObject(
                ClFlowActions.createUser(requestPayload.toString(), requestHeaders));
        Assert.assertEquals(createUserResponseJSON.getInt("statusCode"), 201, "Status code is other than 201");

        // Getting customerId by mobile number
        customerId = NonRiskDatabaseMiner.findCustomerIdByMobileNumber(mobileNumber);

        // PD and ED details submission
        CommonFunctions.pDAndEdSubmission(customerId, pan, requestHeaders);

        // User journey token
        String User_Journey_token = AuthUtility.getAccessToken(TokenScopeType.USER_JOURNEY);
        userjourneyrequestHeaders.put("Authorization", "Bearer " + User_Journey_token);

        if (merchantId == null) {
            merchantId = "a70ce9c4-881a-405d-834a-4a18554fb33a";
        }

        NonRiskDatabaseMiner.updateMerchantInPreApprove(customerId, merchantId);

        // Cibil mock
        int score = 700;
        CibilDatabaseMiner.mockCibil(customerId, pan, email, score, mobileNumber);

        // PreApproveV2Stage1
        JSONObject preApproveR1ResponseBody = new JSONObject(
                ClFlowAction.preapproveV2stage(customerId, userjourneyrequestHeaders).getBody());

        // Customer is referred at R1
        if (preApproveR1ResponseBody.getInt("decision_code") == 4) {
            Logger.log("The customer is referred at PreApproveV2Stage1 ");
        }

        // Customer is rejected at R1
        if (preApproveR1ResponseBody.getInt("decision_code") == 3) {
            String[] queryParams = {customerId, "PreApproveV2Stage1"};
            String riskworkflowid = DBUtils.fetchDataFromDB(DBQueries.GET_RISKWORKFLOWID_QUERY, queryParams);
            Object dagResponse = DBUtils.fetchDataFromDB(DBQueries.GET_DAG_QUERY, riskworkflowid);
            JSONObject response = new JSONObject(dagResponse.toString()).getJSONObject("response");
            Iterator<String> keys = response.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equalsIgnoreCase("label") && key != "") {
                    Logger.log("The customer is rejected due to : " + response.get("label") + " Checkpoint");
                }
            }
            return;
        }

        // Verifying all scorecards are stored in scorecard table.
        String[] queryParams = {customerId, "PreApproveV2Stage1"};
        String riskworkflowid = DBUtils.fetchDataFromDB(DBQueries.GET_RISKWORKFLOWID_QUERY, queryParams);
        String dagResponse = DBUtils.fetchDataFromDB(DBQueries.GET_DAG_QUERY, riskworkflowid);
        Logger.log(dagResponse);
        ArrayList<String> expectedScorecard = CommonFunctions.scoreCardChecks(dagResponse);
        ArrayList<HashMap<String, String>> actualScorecard = DBUtils
                .getMultipleColumnData(DBUtils.createQuery(DBQueries.GET_SCORECARD_RISKWORKFLOW_QUERY, riskworkflowid));
        boolean flag = CommonFunctions.sanityChecks(actualScorecard, expectedScorecard, score);

        if (flag == false) {
            Logger.log("The sanity check is failed at PreApproveV2Stage1");
            return;
        }

        // Mocking netbanking Perfios
        CibilDatabaseMiner.mockNetBankingPerfiosData(customerId, mobileNumber);

        // Post Bank details
        JSONObject postBankDetailsRequestBody = new JSONObject(
                "{\"CustomerId\":\"\",\"AccountHoldersName\": \"Maruti HIrave\",\"AccountNumber\": \"\",\"AccountType\": \"Savings\",\"BankId\": \"UTIB\",\"BankName\": \"Axis Bank\",\"CreatedOn\": \"\",\"CustomBankName\": null,\"IFSC\": \"UTIB0000009\",\"Source\": \"UserFilled\",\"TrustedMerchantId\": null,\"IsAadhaarLinkedToBankAccount\": null,\"ApplicationId\": null}");
        postBankDetailsRequestBody.put("CustomerId", customerId);
        postBankDetailsRequestBody.put("AccountNumber", BankNumber);
        JSONObject postBankDetailsResponceJSON = new JSONObject(
                MerchantFlowAction.postBankDetails(customerId, postBankDetailsRequestBody.toString(), requestHeaders));
        Assert.assertEquals(postBankDetailsResponceJSON.getInt("statusCode"), 201, "Statuscode is other than 201");

        // PostApproveV2Stage2
        JSONObject preApproveV2Stage2ResponceBody = new JSONObject(
                ClFlowAction.preapproveV2stage(customerId, userjourneyrequestHeaders).getBody());

        if (preApproveV2Stage2ResponceBody.getInt("decision_code") == 3) {
            Logger.log("The application is rejected at PreApproveV2Stage2");
            return;
        }

        // Customer is referred at R2
        if (preApproveV2Stage2ResponceBody.getInt("decision_code") == 4) {
            Logger.log("The customer is referred at PreApproveV2Stage2 ");
        }

        // Verifying all scorecards are stored in scorecard table.
        String[] stage2QuerParams = {customerId, "PreApproveV2Stage2"};
        riskworkflowid = DBUtils.fetchDataFromDB(DBQueries.GET_RISKWORKFLOWID_QUERY, stage2QuerParams);
        dagResponse = DBUtils.fetchDataFromDB(DBQueries.GET_DAG_QUERY, riskworkflowid);
        Logger.log(dagResponse);
        ArrayList<HashMap<String, String>> expectedScorecardByenrichment = DBUtils
                .getMultipleColumnData(DBUtils.createQuery(DBQueries.GET_SCORECARD_RISKWORKFLOW_QUERY, riskworkflowid));
        flag = CommonFunctions.scoreCardChecksByEnrichment(dagResponse, expectedScorecardByenrichment, score);

        // KYC and Mandate Flow
        CommonFunctions.KycMandateFunction(customerId, requestHeaders);
    }

    @Test(description = "Stage 1 Salaried check tests", groups = {Group.CL_FLOW_SALARY}, dataProvider = "STAGE1_SALARIED_CHECK", dataProviderClass = CLFlowDataProvider.class)
    @Severity(SeverityLevel.BLOCKER)
    @Description("Stage 1 Salaried check tests - Pass the merchant id's it will fetch the salary cut off from the dag and verifies")
    public void verify_stage1_salaried_check(String MerchantId, String testCaseName, int monthlyIncome, String result) throws Exception {
        String mobileNumber = CommonUtility.randomMobileNumber();
        String email = mobileNumber + "@yopmail.com";
        String customerId;
        String pan = CommonUtility.randomPAN();
        String merchantId = MerchantId;

        String access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

        // Preparing the common HTTP header information
        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("Authorization", "Bearer " + access_token);

        // Preparing the common HTTP header information
        Map<String, String> userjourneyrequestHeaders = new HashMap<>();
        userjourneyrequestHeaders.put("Content-Type", "application/json");

        // Create customer
        JSONObject requestPayload = new JSONObject("{\"emailAddress\": \"\",\"MobileNumber\": \"\"}}");
        requestPayload.put("emailAddress", email);
        requestPayload.put("MobileNumber", "91" + mobileNumber);
        JSONObject createUserResponseJSON = new JSONObject(
                ClFlowActions.createUser(requestPayload.toString(), requestHeaders));
        Assert.assertEquals(createUserResponseJSON.getInt("statusCode"), 201, "Status code is other than 201");

        // Getting customerId by mobile number
        customerId = NonRiskDatabaseMiner.findCustomerIdByMobileNumber(mobileNumber);

        // PD and ED details submission
        JSONObject postSocialDetailsRequestBody = new JSONObject(
                "{\"Gender\": \"Male\",\"CustomerId\": \"\",\"FullName\": \"Maruti Hirave\",\"DateOfBirth\": \"1988-06-01\",\"PinCode\": \"788001\",\"Location\": \"Bang\",\"City\": \"Bangalore\",\"Area\": \"Bangalore South\",\"Pan\": \"\"}");
        postSocialDetailsRequestBody.put("CustomerId", customerId);
        postSocialDetailsRequestBody.put("Pan", pan);
        JSONObject postSocialDetailsResponseJSON = new JSONObject(
                (MerchantFlowAction.postPersonalDetails(postSocialDetailsRequestBody.toString(), requestHeaders)));
        Assert.assertEquals(postSocialDetailsResponseJSON.getInt("statusCode"), 201, "Status code is other than 201");

        // Posting the Employment details
        JSONObject postEdDetailsRequestBody = new JSONObject(
                "{\"CustomerId\": \"\",\"EmploymentStatus\": \"Salaried\",\"CurrentEmployer\": \"Camden\",\"NetMonthlyIncome\":\"\" ,\"LiveWith\": \"NoOne\",  \"MinimumMonthlySpending\": 0,\"MaximumMonthlySpending\": 5000}");
        postEdDetailsRequestBody.put("CustomerId", customerId);
        postEdDetailsRequestBody.put("NetMonthlyIncome", monthlyIncome);
        JSONObject postEdDetailsResponseJSON = new JSONObject((MerchantFlowAction.postEmploymentDetails(customerId,
                postEdDetailsRequestBody.toString(), requestHeaders)));
        Assert.assertEquals(postEdDetailsResponseJSON.getInt("statusCode"), 201, "Statuscode is other than 201");

        // User journey token
        String User_Journey_token = AuthUtility.getAccessToken(TokenScopeType.USER_JOURNEY);
        userjourneyrequestHeaders.put("Authorization", "Bearer " + User_Journey_token);

        if (merchantId == null) {
            merchantId = "a70ce9c4-881a-405d-834a-4a18554fb33a";
        }

        NonRiskDatabaseMiner.updateMerchantInPreApprove(customerId, merchantId);

        // Cibil mock
        int score = 710;
        CibilDatabaseMiner.mockCibil(customerId, pan, email, score, mobileNumber);

        // PreApproveV2Stage1
        JSONObject preApproveR1ResponseBody = new JSONObject(
                ClFlowAction.preapproveV2stage(customerId, userjourneyrequestHeaders).getBody());

        SoftAssert checks = new SoftAssert();
        checks.assertEquals(preApproveR1ResponseBody.getInt("decision_code"), Integer.parseInt(result));

        // Customer is referred at R1
        if (preApproveR1ResponseBody.getInt("decision_code") == 4) {
            Logger.log("The customer is referred at PreApproveV2Stage1 ");
        }

        // Customer is rejected at R1
        String riskworkflowid;
        if (preApproveR1ResponseBody.getInt("decision_code") == 3) {
            String[] queryParams = {customerId, "PreApproveV2Stage1"};
            riskworkflowid = DBUtils.fetchDataFromDB(DBQueries.GET_RISKWORKFLOWID_QUERY, queryParams);
            Object dagResponse = DBUtils.fetchDataFromDB(DBQueries.GET_DAG_QUERY, riskworkflowid);
            JSONObject response = new JSONObject(dagResponse.toString()).getJSONObject("response");
            Iterator<String> keys = response.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equalsIgnoreCase("label") && key != "") {
                    Logger.log("The customer is rejected due to : " + response.get("label") + " Checkpoint");
                }
            }
            return;
        }
        // Customer is referred at R1
        if (preApproveR1ResponseBody.getInt("decision_code") == 2) {
            Logger.log("The customer is accepted at PreApproveV2Stage1 ");
        }

    }

    @Test(description = "Stage 1 Min_Age and max age check tests", groups = {Group.CL_FLOW_MIN_MAX_AGE_CHECK}, dataProvider = "STAGE1_MIN_MAX_AGE_CHECK", dataProviderClass = CLFlowDataProvider.class)
    @Severity(SeverityLevel.BLOCKER)
    @Description("Stage 1 MinAge and max age check tests - Pass the merchant id's it will fetch the min_age cut off from the dag and verifies")
    public void verify_stage1_age_check(String MerchantId, String testCaseName, boolean isMaxAge, String dob, boolean result) throws Exception {
        String mobileNumber = CommonUtility.randomMobileNumber();
        String email = mobileNumber + "@yopmail.com";
        String customerId;
        String pan = CommonUtility.randomPAN();
        String merchantId = MerchantId;

        String access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

        // Preparing the common HTTP header information
        Map<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("Authorization", "Bearer " + access_token);

        // Preparing the common HTTP header information
        Map<String, String> userjourneyrequestHeaders = new HashMap<>();
        userjourneyrequestHeaders.put("Content-Type", "application/json");

        // Create customer
        JSONObject requestPayload = new JSONObject("{\"emailAddress\": \"\",\"MobileNumber\": \"\"}}");
        requestPayload.put("emailAddress", email);
        requestPayload.put("MobileNumber", "91" + mobileNumber);
        JSONObject createUserResponseJSON = new JSONObject(
                ClFlowActions.createUser(requestPayload.toString(), requestHeaders));
        Assert.assertEquals(createUserResponseJSON.getInt("statusCode"), 201, "Status code is other than 201");

        // Getting customerId by mobile number
        customerId = NonRiskDatabaseMiner.findCustomerIdByMobileNumber(mobileNumber);

        // PD and ED details submission
        JSONObject postSocialDetailsRequestBody = new JSONObject(
                "{\"Gender\": \"Male\",\"CustomerId\": \"\",\"FullName\": \"Maruti Hirave\",\"DateOfBirth\": \"\",\"PinCode\": \"788001\",\"Location\": \"Bang\",\"City\": \"Bangalore\",\"Area\": \"Bangalore South\",\"Pan\": \"\"}");
        postSocialDetailsRequestBody.put("CustomerId", customerId);
        postSocialDetailsRequestBody.put("Pan", pan);
        postSocialDetailsRequestBody.put("DateOfBirth", dob);
        JSONObject postSocialDetailsResponseJSON = new JSONObject(
                (MerchantFlowAction.postPersonalDetails(postSocialDetailsRequestBody.toString(), requestHeaders)));
        Assert.assertEquals(postSocialDetailsResponseJSON.getInt("statusCode"), 201, "Status code is other than 201");

        // Posting the Employment details
        JSONObject postEdDetailsRequestBody = new JSONObject(
                "{\"CustomerId\": \"\",\"EmploymentStatus\": \"Salaried\",\"CurrentEmployer\": \"Camden\",\"NetMonthlyIncome\":90000 ,\"LiveWith\": \"NoOne\",  \"MinimumMonthlySpending\": 0,\"MaximumMonthlySpending\": 5000}");
        postEdDetailsRequestBody.put("CustomerId", customerId);

        JSONObject postEdDetailsResponseJSON = new JSONObject((MerchantFlowAction.postEmploymentDetails(customerId,
                postEdDetailsRequestBody.toString(), requestHeaders)));
        Assert.assertEquals(postEdDetailsResponseJSON.getInt("statusCode"), 201, "Statuscode is other than 201");

        // User journey token
        String User_Journey_token = AuthUtility.getAccessToken(TokenScopeType.USER_JOURNEY);
        userjourneyrequestHeaders.put("Authorization", "Bearer " + User_Journey_token);

        if (merchantId == null) {
            merchantId = "a70ce9c4-881a-405d-834a-4a18554fb33a";
        }

        NonRiskDatabaseMiner.updateMerchantInPreApprove(customerId, merchantId);

        // Cibil mock
        int score = 710;
        CibilDatabaseMiner.mockCibil(customerId, pan, email, score, mobileNumber);

        // PreApproveV2Stage1
        JSONObject preApproveR1ResponseBody = new JSONObject(
                ClFlowAction.preapproveV2stage(customerId, userjourneyrequestHeaders).getBody());

        SoftAssert checks = new SoftAssert();
        checks.assertEquals(preApproveR1ResponseBody.getInt("decision_code"), result);

        SoftAssert check = new SoftAssert();

//        For verifying individual check result
        String[] queryParams = {customerId, "PreApproveV2Stage1"};
        JSONObject dag = new JSONObject(DBUtils.fetchDataFromDB(DBQueries.GET_DAG_BY_CUSTOMER_ID_JOURNEY_QUERY, queryParams));

        boolean checkResult = false;

        if (isMaxAge)
            checkResult = dag.getJSONObject("rules").getJSONObject("MaxAgeCheck").getBoolean("result");
        else
            checkResult = dag.getJSONObject("rules").getJSONObject("MinAgeCheck").getBoolean("result");

        check.assertEquals(checkResult, result);

        check.assertAll();

        // Customer is referred at R1
        if (preApproveR1ResponseBody.getInt("decision_code") == 4) {
            Logger.log("The customer is referred at PreApproveV2Stage1 ");
        }

        // Customer is rejected at R1
        String riskworkflowid;
        if (preApproveR1ResponseBody.getInt("decision_code") == 3) {
            queryParams = new String[]{customerId, "PreApproveV2Stage1"};
            riskworkflowid = DBUtils.fetchDataFromDB(DBQueries.GET_RISKWORKFLOWID_QUERY, queryParams);
            Object dagResponse = DBUtils.fetchDataFromDB(DBQueries.GET_DAG_QUERY, riskworkflowid);
            JSONObject response = new JSONObject(dagResponse.toString()).getJSONObject("response");
            Iterator<String> keys = response.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equalsIgnoreCase("label") && key != "") {
                    Logger.log("The customer is rejected due to : " + response.get("label") + " Checkpoint");
                }
            }
            return;
        }
        // Customer is referred at R1
        if (preApproveR1ResponseBody.getInt("decision_code") == 2) {
            Logger.log("The customer is accepted at PreApproveV2Stage1 ");
        }

    }

}
