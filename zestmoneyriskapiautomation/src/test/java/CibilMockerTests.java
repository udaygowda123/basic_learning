package test.java;

import io.qameta.allure.*;
import main.java.databaseHelper.CibilDatabaseMiner;
import main.java.databaseHelper.NonRiskDatabaseMiner;
import main.java.interfaces.Group;

import main.java.utils.listners.Logger;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CibilMockerTests extends Commons {

	@Test(description = "Mocking the cibil transaction and segment data for a given customer", groups = {Group.NORMAL_CIBIL_MOCK})
	@Severity(SeverityLevel.BLOCKER)
	@Description("Mocking the cibil transaction and segment data for a given customer")
	public void mock_cibil_data_in_database() throws Exception {

		boolean flag = true;

		// Reading the environment name from command line and setting it

		String input_mobileNumber = System.getProperty("phoneNumber");

		String mobileNumber = null;

		if(input_mobileNumber == null || input_mobileNumber == ""){
			flag = false;
			Logger.log("Invalid Mobile Number");
		}
		else {
			mobileNumber = input_mobileNumber;
		}

		String customerId = NonRiskDatabaseMiner.findCustomerIdByMobileNumber(mobileNumber);
		String pan = NonRiskDatabaseMiner.findPanByCustomerId(customerId);
		int score=850;
		String email=NonRiskDatabaseMiner.findEmailByCustomerId(customerId);

		if (flag)
			CibilDatabaseMiner.mockCibil(customerId, pan, email, score, mobileNumber);
		else
			Assert.fail("Invalid input!");
	}


    @Test(description = "Mocking for trip money", groups = {Group.TRIP_MONEY_CIBIL_MOCK})
    @Severity(SeverityLevel.BLOCKER)
    @Description("Mocking for trip money")
    public void mock_data_in_database_for_trip_money() throws Exception {

        String mobileNumber = "9742052102";
        String customerId = NonRiskDatabaseMiner.findCustomerIdByMobileNumber(mobileNumber);
        String pan = NonRiskDatabaseMiner.findPanByCustomerId(customerId);

        NonRiskDatabaseMiner.resetTripMoneyUserForRisk(mobileNumber);
        CibilDatabaseMiner.removeCLforTripMoney(customerId);

        int score = 849;
    
        CibilDatabaseMiner.mockCibilForTripMoney(customerId, pan, score, mobileNumber);

    }
    
    @Test(description = "Mocking net banking perfios for a customer", groups = {Group.PERFIOS})
    @Severity(SeverityLevel.BLOCKER)
    @Description("Mocking netbanking perfios data")
    public static void MockNetBankingPerfios() {

    	boolean flag = true;

    	// Reading the environment name from command line and setting it

    	String input_mobileNumber = System.getProperty("phoneNumber");
    	String mobileNumber = null;

    	try {
    		if(input_mobileNumber == null || input_mobileNumber == ""){
    			flag = false;
    			Logger.log("Invalid Mobile Number");
    		}
    		else {
    			mobileNumber = input_mobileNumber;
    		}

    		String customerId = NonRiskDatabaseMiner.findCustomerIdByMobileNumber(mobileNumber);

    		if (flag)
    			CibilDatabaseMiner.mockNetBankingPerfiosData(customerId, mobileNumber);
    		else
    			Assert.fail("Invalid input!");

    	} catch (Exception e) {
    		e.getMessage();
    	}
    }

}
