package test.java;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import main.java.authService.utility.AuthUtility;
import main.java.databaseHelper.CibilDatabaseMiner;
import main.java.databaseHelper.NonRiskDatabaseMiner;
import main.java.databaseHelper.RiskDatabaseMiner;
import main.java.enums.Constants;
import main.java.enums.EngineType;
import main.java.enums.TokenScopeType;
import main.java.interfaces.Group;
import main.java.ruleExecuterService.actions.RuleExecutorAction;
import main.java.rule_engine.trip_money.actions.TripMoneyAction;
import main.java.utils.CommonUtility;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import test.resources.dataproviders.TripMoneyDataprovider;
import java.util.HashMap;
import java.util.Map;

public class TripMoneyTests extends Commons {

    @Test(description = "Trip Money Stage 1 tests", groups = {Group.TRIP_MONEY_SANITY}, dataProvider="TRIP_MONEY_STAGE1", dataProviderClass = TripMoneyDataprovider.class)
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verifying credit limit generation for trip money users after running R1")
    public void verify_trip_money_stage1(String testCaseName, int cibilV3score, boolean expectedResult) throws Exception {

        String merchantId = "3c528762-7485-4d93-bd14-488bc14596b4";
        String customerId;
        String mobileNumber = CommonUtility.randomMobileNumber();
        String pan = CommonUtility.randomPAN();
        String accountId = pan;
        String email = mobileNumber + "@yopmail.com";

        JSONObject linkRequestBody = new JSONObject("{\"mobile\":\"\",\"partnerId\":\"MMT\"}");
        JSONObject eligibilityRequestBody = new JSONObject("{\"accountId\":\"\",\"pan\":\"\",\"mobile\":\"\",\"email\":\"\",\"dob\":\"1988-06-01\",\"firstName\":\"Abhishek\",\"lastName\":\"Kumar\",\"gender\":\"MALE\",\"residenceType\":\"OWNED\",\"employmentType\":\"SALARIED\",\"monthlyIncome\":\"80001\",\"employerName\":\"Zest\",\"employedSince\":\"2020-03-11\",\"livingWith\":\"NO_ONE\",\"monthlyExpenses\":\"5000\",\"currentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Koramangal\",\"addressLine2\":\"Ramagondanahalli\"},\"permanentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Galaxy apartment\",\"addressLine2\":\"Koramangala\"}}");

        linkRequestBody.put("mobile", mobileNumber);

        eligibilityRequestBody.put("accountId", accountId);
        eligibilityRequestBody.put("pan", pan);
        eligibilityRequestBody.put("mobile", mobileNumber);
        eligibilityRequestBody.put("email", email);

        String access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Authorization", "Bearer " + access_token);
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("x-api-key","TdnSW3mdJ36E0pJZkvwIw9mOXlnzztUC2ZErXZHQ");
        requestHeaders.put("verificationid", "tripmoney");

//        Running Link API
        JSONObject linkAccountResponseJSON = new JSONObject((TripMoneyAction.linkAccount(accountId, linkRequestBody.toString(), requestHeaders)).getBody());
        Assert.assertEquals(linkAccountResponseJSON.getJSONObject("data").getJSONObject("account").getString("accountStatus"), "LINKED","Account status is not set to Linked");

//        Running eligibility api
        JSONObject eligibilityResponseJSON = new JSONObject((TripMoneyAction.eligibility(accountId,eligibilityRequestBody.toString(),requestHeaders).getBody()));
        Assert.assertEquals(eligibilityResponseJSON.getString("responseStatus"),"SUCCESS", "responseStatus is not set to SUCCESS");

        customerId = eligibilityResponseJSON.getJSONObject("data").getJSONObject("loan").getString("loanRefNum");

//        Clearing current R1 run data
        Thread.sleep(Constants.TRIPMONEY_WAIT);
        NonRiskDatabaseMiner.resetTripMoneyUserForRisk(mobileNumber);
        CibilDatabaseMiner.removeCLforTripMoney(customerId);

//        Setting up Cibil Data
        Thread.sleep(Constants.CIBIL_WAIT);
        CibilDatabaseMiner.clearExistingTransactionByCustomerId(customerId);
        CibilDatabaseMiner.mockCibilForTripMoney(customerId, pan, cibilV3score, mobileNumber);

//        Running TripMoney R1
        access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeadersStage1 = new HashMap<String, String>();
        requestHeadersStage1.put("Authorization", "Bearer " + access_token);
        requestHeadersStage1.put("Content-Type", "application/json");

        JSONArray results = new JSONArray(TripMoneyAction.tripMoneyStage1(customerId,merchantId, requestHeadersStage1).getBody());

        SoftAssert checks = new SoftAssert();

        for (int i =0; i< results.length(); i++){
            checks.assertEquals(results.getJSONObject(i).getBoolean("result"), expectedResult);
            if (expectedResult)
                checks.assertTrue(results.getJSONObject(i).getJSONObject("credit_limit").getInt("approved_amt")>=7000, "Approved_amt is not >= 7000");
        }

//        Verifying CLR1, CLR2, CLR3, Affordable amount calculation
        JSONObject dag = new JSONObject(RiskDatabaseMiner.getCustomerJourneyDag(customerId, EngineType.TRIPMONEYSTAGE1.label));

        JSONObject tripMoneyCreditLimitCheckData = dag.getJSONObject("rules").getJSONObject("TripMoneyCreditLimitCheck").getJSONObject("data");

        int MIN_TERM_IN_MONTHS = tripMoneyCreditLimitCheckData.getInt("min_term_in_months");
        double RATIO_TOTAL_EMI_TO_NMI = tripMoneyCreditLimitCheckData.getDouble("ratio_total_emi_to_nmi");
        double RATIO_ZEST_EMI_TO_NMI = tripMoneyCreditLimitCheckData.getDouble("ratio_zest_emi_to_nmi");
        double MAX_INTEREST_RATE = tripMoneyCreditLimitCheckData.getDouble("max_interest_rate");
        double perMonthEMI = tripMoneyCreditLimitCheckData.getDouble("per_month_emi");
        double monthlyIncome = tripMoneyCreditLimitCheckData.getDouble("CIBILIncomeEstimationCheck");

        double totalMonthlySpending = eligibilityRequestBody.getDouble("monthlyExpenses");

        double actualClr1 = tripMoneyCreditLimitCheckData.getDouble("clR1");
        double actualClr2 = tripMoneyCreditLimitCheckData.getDouble("clR2");
        double actualClr3 = tripMoneyCreditLimitCheckData.getDouble("clR3");

        double actualInterest = tripMoneyCreditLimitCheckData.getDouble("interest");
        double actualAffordableAmt = tripMoneyCreditLimitCheckData.getDouble("affordable_amt");

        double expectedInterest = MIN_TERM_IN_MONTHS / (1.0 + MAX_INTEREST_RATE * MIN_TERM_IN_MONTHS);
        double clR1 = RATIO_ZEST_EMI_TO_NMI * monthlyIncome * expectedInterest;
        double clR2 = ((monthlyIncome - totalMonthlySpending - perMonthEMI) * expectedInterest);
        double clR3 = ((RATIO_TOTAL_EMI_TO_NMI * monthlyIncome) - perMonthEMI) * expectedInterest;
        double affordableAmount = Math.min(clR1, Math.min(clR2, clR3));

//        rounding to 2 decimal places
        double scale = Math.pow(10, 2);
        affordableAmount = Math.round(affordableAmount * scale) / scale;

        checks.assertEquals(actualClr1, clR1);
        checks.assertEquals(actualClr2, clR2);
        checks.assertEquals(actualClr3, clR3);

        checks.assertEquals(actualAffordableAmt, affordableAmount);
        checks.assertEquals(actualInterest, expectedInterest);

//        Verifying enrichment
        JSONObject customerData = new JSONObject(RiskDatabaseMiner.getCustomerData(customerId));

        if(cibilV3score==-1) {
            JSONObject l0_score = customerData.getJSONObject("score").getJSONObject("l0_score");

            int zest_l0_score = Integer.parseInt(l0_score.getString("zest_l0_score"));
            int version = Integer.parseInt(l0_score.getString("version"));
            boolean is_thin_customer = l0_score.getBoolean("is_thin_customer");
            int cibil_score = Integer.parseInt(l0_score.getString("cibil_score"));
            String cust_type = l0_score.getString("cust_type");

            checks.assertEquals(cust_type, "NTC");
            checks.assertEquals(cibil_score, -1);
            checks.assertEquals(is_thin_customer, true);
            checks.assertEquals(version, 5);
            checks.assertTrue(zest_l0_score > 0);
        } else{
            JSONObject l0_thick_v5 = customerData.getJSONObject("score").getJSONObject("l0_thick_v5");

            int zest_l0v5_score = Integer.parseInt(l0_thick_v5.getString("zest_l0v5_score"));
            int version = Integer.parseInt(l0_thick_v5.getString("version"));
            boolean is_thin_customer = l0_thick_v5.getBoolean("is_thin_customer");
            int cibil_score = Integer.parseInt(l0_thick_v5.getString("cibil_score"));

            checks.assertEquals(cibil_score, cibilV3score);
            checks.assertEquals(is_thin_customer, false);
            checks.assertEquals(version, 5);
            checks.assertTrue(zest_l0v5_score > 0);
        }

        checks.assertAll();
    }

    @Test(description = "Trip Money Stage 1 test for customer with no score segment present", groups = {Group.TRIP_MONEY_SANITY})
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verifying No credit limit is generated for trip money users having no score segment after running R1")
    public void verify_trip_money_stage1_for_customer_not_having_score_segment() throws Exception {

        String merchantId = "3c528762-7485-4d93-bd14-488bc14596b4";
        String customerId;
        String mobileNumber = CommonUtility.randomMobileNumber();
        String pan = CommonUtility.randomPAN();
        String accountId = pan;
        String email = mobileNumber + "@yopmail.com";

        JSONObject linkRequestBody = new JSONObject("{\"mobile\":\"\",\"partnerId\":\"MMT\"}");
        JSONObject eligibilityRequestBody = new JSONObject("{\"accountId\":\"\",\"pan\":\"\",\"mobile\":\"\",\"email\":\"\",\"dob\":\"1988-06-01\",\"firstName\":\"Abhishek\",\"lastName\":\"Kumar\",\"gender\":\"MALE\",\"residenceType\":\"OWNED\",\"employmentType\":\"SALARIED\",\"monthlyIncome\":\"80001\",\"employerName\":\"Zest\",\"employedSince\":\"2020-03-11\",\"livingWith\":\"NO_ONE\",\"monthlyExpenses\":\"5000\",\"currentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Koramangal\",\"addressLine2\":\"Ramagondanahalli\"},\"permanentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Galaxy apartment\",\"addressLine2\":\"Koramangala\"}}");

        linkRequestBody.put("mobile", mobileNumber);

        eligibilityRequestBody.put("accountId", accountId);
        eligibilityRequestBody.put("pan", pan);
        eligibilityRequestBody.put("mobile", mobileNumber);
        eligibilityRequestBody.put("email", email);

        String access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Authorization", "Bearer " + access_token);
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("x-api-key","TdnSW3mdJ36E0pJZkvwIw9mOXlnzztUC2ZErXZHQ");
        requestHeaders.put("verificationid", "tripmoney");

//        Running Link API
        JSONObject linkAccountResponseJSON = new JSONObject((TripMoneyAction.linkAccount(accountId, linkRequestBody.toString(), requestHeaders)).getBody());
        Assert.assertEquals(linkAccountResponseJSON.getJSONObject("data").getJSONObject("account").getString("accountStatus"), "LINKED","Account status is not set to Linked");

//        Running eligibility api
        JSONObject eligibilityResponseJSON = new JSONObject((TripMoneyAction.eligibility(accountId,eligibilityRequestBody.toString(),requestHeaders).getBody()));
        Assert.assertEquals(eligibilityResponseJSON.getString("responseStatus"),"SUCCESS", "responseStatus is not set to SUCCESS");

        customerId = eligibilityResponseJSON.getJSONObject("data").getJSONObject("loan").getString("loanRefNum");

//        Clearing current R1 run data
        NonRiskDatabaseMiner.resetTripMoneyUserForRisk(mobileNumber);
        CibilDatabaseMiner.removeCLforTripMoney(customerId);

//        Setting up Cibil Data
        Thread.sleep(Constants.CIBIL_WAIT);
        CibilDatabaseMiner.mockCibilForTripMoneyNoScoreSegment(customerId);

//        Running TripMoney R1
        access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeadersStage1 = new HashMap<String, String>();
        requestHeadersStage1.put("Authorization", "Bearer " + access_token);
        requestHeadersStage1.put("Content-Type", "application/json");

        JSONArray results = new JSONArray(TripMoneyAction.tripMoneyStage1(customerId,merchantId, requestHeadersStage1).getBody());

        SoftAssert checks = new SoftAssert();

        for (int i =0; i< results.length(); i++){
            checks.assertFalse(results.getJSONObject(i).getBoolean("result"));
        }

        checks.assertAll();
    }

    @Test(description = "Trip Money Stage 1 Age check tests", groups = {Group.TRIP_MONEY_SANITY}, dataProvider="TRIP_MONEY_STAGE1_AGE_CHECK", dataProviderClass = TripMoneyDataprovider.class)
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verifying age checks for trip money users in R1")
    public void verify_trip_money_stage1_age_check(String testCaseName, boolean isMaxAge, String dob, boolean result) throws Exception {

        String merchantId = "3c528762-7485-4d93-bd14-488bc14596b4";
        String customerId;
        String mobileNumber = CommonUtility.randomMobileNumber();
        String pan = CommonUtility.randomPAN();
        String accountId = pan;
        String email = mobileNumber + "@yopmail.com";
        int cibilV3score = 849;

        JSONObject linkRequestBody = new JSONObject("{\"mobile\":\"\",\"partnerId\":\"MMT\"}");
        JSONObject eligibilityRequestBody = new JSONObject("{\"accountId\":\"\",\"pan\":\"\",\"mobile\":\"\",\"email\":\"\",\"dob\":\"1988-06-01\",\"firstName\":\"Abhishek\",\"lastName\":\"Kumar\",\"gender\":\"MALE\",\"residenceType\":\"OWNED\",\"employmentType\":\"SALARIED\",\"monthlyIncome\":\"80001\",\"employerName\":\"Zest\",\"employedSince\":\"2020-03-11\",\"livingWith\":\"NO_ONE\",\"monthlyExpenses\":\"5000\",\"currentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Koramangal\",\"addressLine2\":\"Ramagondanahalli\"},\"permanentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Galaxy apartment\",\"addressLine2\":\"Koramangala\"}}");

        linkRequestBody.put("mobile", mobileNumber);

        eligibilityRequestBody.put("accountId", accountId);
        eligibilityRequestBody.put("pan", pan);
        eligibilityRequestBody.put("mobile", mobileNumber);
        eligibilityRequestBody.put("email", email);
        eligibilityRequestBody.put("dob", dob);

        String access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Authorization", "Bearer " + access_token);
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("x-api-key","TdnSW3mdJ36E0pJZkvwIw9mOXlnzztUC2ZErXZHQ");
        requestHeaders.put("verificationid", "tripmoney");

//        Running Link API
        JSONObject linkAccountResponseJSON = new JSONObject((TripMoneyAction.linkAccount(accountId, linkRequestBody.toString(), requestHeaders)).getBody());
        Assert.assertEquals(linkAccountResponseJSON.getJSONObject("data").getJSONObject("account").getString("accountStatus"), "LINKED","Account status is not set to Linked");

//        Running eligibility api
        JSONObject eligibilityResponseJSON = new JSONObject((TripMoneyAction.eligibility(accountId,eligibilityRequestBody.toString(),requestHeaders).getBody()));
        Assert.assertEquals(eligibilityResponseJSON.getString("responseStatus"),"SUCCESS", "responseStatus is not set to SUCCESS");

        customerId = eligibilityResponseJSON.getJSONObject("data").getJSONObject("loan").getString("loanRefNum");

//        Clearing current R1 run data
        NonRiskDatabaseMiner.resetTripMoneyUserForRisk(mobileNumber);
        CibilDatabaseMiner.removeCLforTripMoney(customerId);

//        Setting up Cibil Data
        Thread.sleep(Constants.CIBIL_WAIT);
        CibilDatabaseMiner.mockCibilForTripMoney(customerId, pan, cibilV3score, mobileNumber);

//        Running TripMoney R1
        access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeadersStage1 = new HashMap<String, String>();
        requestHeadersStage1.put("Authorization", "Bearer " + access_token);
        requestHeadersStage1.put("Content-Type", "application/json");

        JSONArray results = new JSONArray(TripMoneyAction.tripMoneyStage1(customerId,merchantId, requestHeadersStage1).getBody());

        SoftAssert checks = new SoftAssert();

//        For verifying individual check result
        JSONObject dag = new JSONObject(RiskDatabaseMiner.getCustomerJourneyDag(customerId, EngineType.TRIPMONEYSTAGE1.label));

        boolean checkResult = false;

        if(isMaxAge)
            checkResult = dag.getJSONObject("rules").getJSONObject("MaxAgeCheck").getBoolean("result");
        else
            checkResult = dag.getJSONObject("rules").getJSONObject("MinAgeCheck").getBoolean("result");

        checks.assertEquals(checkResult, result);

        for (int i =0; i< results.length(); i++)
            checks.assertEquals(results.getJSONObject(i).getBoolean("result"), result);

        checks.assertAll();
    }

    @Test(description = "Trip Money Stage 1 Salaried check tests", groups = {Group.TRIP_MONEY_SANITY}, dataProvider="TRIP_MONEY_STAGE1_SALARIED_CHECK", dataProviderClass = TripMoneyDataprovider.class)
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verifying Salaried checks for trip money users in R1")
    public void verify_trip_money_stage1_salary_check(String testCaseName, int monthlyIncome, boolean result) throws Exception {

        String merchantId = "3c528762-7485-4d93-bd14-488bc14596b4";
        String customerId;
        String mobileNumber = CommonUtility.randomMobileNumber();
        String pan = CommonUtility.randomPAN();
        String accountId = pan;
        String email = mobileNumber + "@yopmail.com";
        int cibilV3score = 849;

        JSONObject linkRequestBody = new JSONObject("{\"mobile\":\"\",\"partnerId\":\"MMT\"}");
        JSONObject eligibilityRequestBody = new JSONObject("{\"accountId\":\"\",\"pan\":\"\",\"mobile\":\"\",\"email\":\"\",\"dob\":\"1988-06-01\",\"firstName\":\"Abhishek\",\"lastName\":\"Kumar\",\"gender\":\"MALE\",\"residenceType\":\"OWNED\",\"employmentType\":\"SALARIED\",\"monthlyIncome\":\"80001\",\"employerName\":\"Zest\",\"employedSince\":\"2020-03-11\",\"livingWith\":\"NO_ONE\",\"monthlyExpenses\":\"5000\",\"currentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Koramangal\",\"addressLine2\":\"Ramagondanahalli\"},\"permanentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Galaxy apartment\",\"addressLine2\":\"Koramangala\"}}");

        linkRequestBody.put("mobile", mobileNumber);

        eligibilityRequestBody.put("accountId", accountId);
        eligibilityRequestBody.put("pan", pan);
        eligibilityRequestBody.put("mobile", mobileNumber);
        eligibilityRequestBody.put("email", email);
        eligibilityRequestBody.put("monthlyIncome", monthlyIncome+"");

        String access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Authorization", "Bearer " + access_token);
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("x-api-key","TdnSW3mdJ36E0pJZkvwIw9mOXlnzztUC2ZErXZHQ");
        requestHeaders.put("verificationid", "tripmoney");

//        Running Link API
        JSONObject linkAccountResponseJSON = new JSONObject((TripMoneyAction.linkAccount(accountId, linkRequestBody.toString(), requestHeaders)).getBody());
        Assert.assertEquals(linkAccountResponseJSON.getJSONObject("data").getJSONObject("account").getString("accountStatus"), "LINKED","Account status is not set to Linked");

//        Running eligibility api
        JSONObject eligibilityResponseJSON = new JSONObject((TripMoneyAction.eligibility(accountId,eligibilityRequestBody.toString(),requestHeaders).getBody()));
        Assert.assertEquals(eligibilityResponseJSON.getString("responseStatus"),"SUCCESS", "responseStatus is not set to SUCCESS");

        customerId = eligibilityResponseJSON.getJSONObject("data").getJSONObject("loan").getString("loanRefNum");

//        Clearing current R1 run data
        NonRiskDatabaseMiner.resetTripMoneyUserForRisk(mobileNumber);
        CibilDatabaseMiner.removeCLforTripMoney(customerId);

//        Setting up Cibil Data
        Thread.sleep(Constants.CIBIL_WAIT);
        CibilDatabaseMiner.mockCibilForTripMoney(customerId, pan, cibilV3score, mobileNumber);

//        Running TripMoney R1
        access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeadersStage1 = new HashMap<String, String>();
        requestHeadersStage1.put("Authorization", "Bearer " + access_token);
        requestHeadersStage1.put("Content-Type", "application/json");

        JSONArray results = new JSONArray(TripMoneyAction.tripMoneyStage1(customerId,merchantId, requestHeadersStage1).getBody());

        SoftAssert checks = new SoftAssert();

        for (int i =0; i< results.length(); i++){

            checks.assertEquals(results.getJSONObject(i).getBoolean("result"), result);
        }

        checks.assertAll();
    }

    @Test(description = "Trip Money Stage 1 L0ThickV5 and L0 Version 5 thin customer tests", groups = {Group.TRIP_MONEY_SANITY}, dataProvider="TRIP_MONEY_STAGE1_L0ThickV5_L0V5", dataProviderClass = TripMoneyDataprovider.class)
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verifying credit limit generation for trip money users after running R1 having l0thickv5 and l0 with version 5 for thin cusomer within expected and outside range")
    public void verify_trip_money_stage1_l0thickV5_l0V5(String testCaseName, boolean isThinCustomer, String l0Score, int cibilV3score, boolean expectedResult) throws Exception {

        String merchantId = "3c528762-7485-4d93-bd14-488bc14596b4";
        String customerId;
        String mobileNumber = CommonUtility.randomMobileNumber();
        String pan = CommonUtility.randomPAN();
        String accountId = pan;
        String email = mobileNumber + "@yopmail.com";

        JSONObject linkRequestBody = new JSONObject("{\"mobile\":\"\",\"partnerId\":\"MMT\"}");
        JSONObject eligibilityRequestBody = new JSONObject("{\"accountId\":\"\",\"pan\":\"\",\"mobile\":\"\",\"email\":\"\",\"dob\":\"1988-06-01\",\"firstName\":\"Abhishek\",\"lastName\":\"Kumar\",\"gender\":\"MALE\",\"residenceType\":\"OWNED\",\"employmentType\":\"SALARIED\",\"monthlyIncome\":\"80001\",\"employerName\":\"Zest\",\"employedSince\":\"2020-03-11\",\"livingWith\":\"NO_ONE\",\"monthlyExpenses\":\"5000\",\"currentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Koramangal\",\"addressLine2\":\"Ramagondanahalli\"},\"permanentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Galaxy apartment\",\"addressLine2\":\"Koramangala\"}}");

        linkRequestBody.put("mobile", mobileNumber);

        eligibilityRequestBody.put("accountId", accountId);
        eligibilityRequestBody.put("pan", pan);
        eligibilityRequestBody.put("mobile", mobileNumber);
        eligibilityRequestBody.put("email", email);

        String access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Authorization", "Bearer " + access_token);
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("x-api-key","TdnSW3mdJ36E0pJZkvwIw9mOXlnzztUC2ZErXZHQ");
        requestHeaders.put("verificationid", "tripmoney");

//        Running Link API
        JSONObject linkAccountResponseJSON = new JSONObject((TripMoneyAction.linkAccount(accountId, linkRequestBody.toString(), requestHeaders)).getBody());
        Assert.assertEquals(linkAccountResponseJSON.getJSONObject("data").getJSONObject("account").getString("accountStatus"), "LINKED","Account status is not set to Linked");

//        Running eligibility api
        JSONObject eligibilityResponseJSON = new JSONObject((TripMoneyAction.eligibility(accountId,eligibilityRequestBody.toString(),requestHeaders).getBody()));
        Assert.assertEquals(eligibilityResponseJSON.getString("responseStatus"),"SUCCESS", "responseStatus is not set to SUCCESS");

        customerId = eligibilityResponseJSON.getJSONObject("data").getJSONObject("loan").getString("loanRefNum");

//        Clearing current R1 run data
        NonRiskDatabaseMiner.resetTripMoneyUserForRisk(mobileNumber);
        CibilDatabaseMiner.removeCLforTripMoney(customerId);

//        Setting up Cibil Data
        Thread.sleep(Constants.CIBIL_WAIT);
        CibilDatabaseMiner.clearExistingTransactionByCustomerId(customerId);
        CibilDatabaseMiner.mockCibilForTripMoney(customerId, pan, cibilV3score, mobileNumber);

//        Running TripMoney R1
        access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeadersStage1 = new HashMap<String, String>();
        requestHeadersStage1.put("Authorization", "Bearer " + access_token);
        requestHeadersStage1.put("Content-Type", "application/json");

        JSONArray results = new JSONArray(TripMoneyAction.tripMoneyStage1(customerId,merchantId, requestHeadersStage1).getBody());

        SoftAssert checks = new SoftAssert();

        for (int i =0; i< results.length(); i++){
            checks.assertTrue(results.getJSONObject(i).getBoolean("result"));
            checks.assertTrue(results.getJSONObject(i).getJSONObject("credit_limit").getInt("approved_amt")>=7000, "Approved_amt is not >= 7000");
        }

//        Preparing Rule executer api request body
        String ruleRequestBody = "{\"record\":{\"data\":{}},\"rules\":[{\"name\":\"TripMoneyCreditLimitGenerate\",\"params\":null,\"aggr\":[{\"label\":\"CIBILIncomeEstimationV2\",\"result\":true,\"final_states\":[],\"data\":{}},{\"label\":\"TripmoneyCIBILScoreCheck\",\"result\":true,\"status_code\":null,\"data\":{\"is_thin_customer\":false,\"cibil_score\":900,\"cibil_version\":\"V2\"}},{\"label\":\"FraudScorecardCheck\",\"result\":null,\"status_code\":\"ZMRSK921\",\"data\":{\"version\":null,\"zest_fraud_score\":null}},{\"label\":\"CIBILMobileUnenrichedCheck\",\"result\":true,\"status_code\":null,\"params\":null,\"data\":{\"mobile\":\"917935975582\",\"label\":\"in_unenriched\"}}]}]}";
        JSONObject ruleRequestBodyJSON = new JSONObject(ruleRequestBody);

//        extracting customer enrichment data and setting it into rule executer body
        JSONObject customerData = new JSONObject(RiskDatabaseMiner.getCustomerData(customerId));
        JSONObject cibil_income_estimation_v2 = customerData.getJSONObject("cibil").getJSONObject("cibil_income_estimation_v2");
        ruleRequestBodyJSON.getJSONObject("record").put("data", customerData);

        JSONArray aggrRules = ruleRequestBodyJSON.getJSONArray("rules").getJSONObject(0).getJSONArray("aggr");

        boolean found = false;
        for(int i=0; i<aggrRules.length(); i++){
            if(aggrRules.getJSONObject(i).getString("label").equals("CIBILIncomeEstimationV2")){
                aggrRules.getJSONObject(i).put("data", cibil_income_estimation_v2);
                found = true;
                break;
            }
        }

        Assert.assertTrue(found, "cibil_income_estimation_v2 is missing in aggr array of TripMoneyCreditLimitGenerate Rule");

//        For thin customer setting lo with version 5 score into the record object and for thick customer setting lothickv5 to record object
        if(isThinCustomer) {
            ruleRequestBodyJSON.getJSONObject("record").getJSONObject("data").getJSONObject("score").getJSONObject("l0_score").put("zest_l0_score", l0Score);

//            setting TripmoneyCIBILScoreCheck data's cibil_score to -1 and is_thin_customer to true
            boolean foundTripmoneyCIBILScoreCheck = false;
            for(int i=0; i<aggrRules.length(); i++){
                if(aggrRules.getJSONObject(i).getString("label").equals("TripmoneyCIBILScoreCheck")){
                    aggrRules.getJSONObject(i).getJSONObject("data").put("is_thin_customer", true);
                    aggrRules.getJSONObject(i).getJSONObject("data").put("cibil_score", cibilV3score);
                    foundTripmoneyCIBILScoreCheck = true;
                    break;
                }
            }

            Assert.assertTrue(foundTripmoneyCIBILScoreCheck, "TripmoneyCIBILScoreCheck is missing in aggr array of TripMoneyCreditLimitGenerate Rule");

        }
        else
            ruleRequestBodyJSON.getJSONObject("record").getJSONObject("data").getJSONObject("score").getJSONObject("l0_thick_v5").put("zest_l0v5_score", l0Score);

        JSONObject dag = new JSONObject(RiskDatabaseMiner.getCustomerJourneyDag(customerId, EngineType.TRIPMONEYSTAGE1.label));
        String journeyId = dag.getString("journey_id");

        String access_token_internal_services = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeadersRuleExecutor = new HashMap<String, String>();
        requestHeadersRuleExecutor.put("Authorization", "Bearer " + access_token_internal_services);
        requestHeadersRuleExecutor.put("Content-Type", "application/json");

        JSONObject ruleExecutorResponseBody = new JSONObject(RuleExecutorAction.runRuleEngine(journeyId, ruleRequestBodyJSON.toString(), requestHeadersRuleExecutor).getBody());
        JSONArray creditLimitLists = ruleExecutorResponseBody.getJSONArray("responses").getJSONObject(0).getJSONObject("data").getJSONArray("credit_limit");

        for (int i =0; i< creditLimitLists.length(); i++)
            checks.assertEquals(creditLimitLists.getJSONObject(i).getBoolean("result"), expectedResult);

        checks.assertAll();
    }

    @Test(description = "Trip Money Stage 1 CIBIL Monthly Income tests", groups = {Group.TRIP_MONEY_SANITY}, dataProvider="TRIP_MONEY_STAGE1_CibilNetMonthlyIncome", dataProviderClass = TripMoneyDataprovider.class)
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verifying credit limit generation for trip money users after running R1 having CIBIL Net monthly income within expected and outside range")
    public void verify_trip_money_stage1_cibilMonthlyIncome(String testCaseName, double cibilNetMonthlyIncome, int cibilV3score, boolean expectedResult) throws Exception {

        String merchantId = "3c528762-7485-4d93-bd14-488bc14596b4";
        String customerId;
        String mobileNumber = CommonUtility.randomMobileNumber();
        String pan = CommonUtility.randomPAN();
        String accountId = pan;
        String email = mobileNumber + "@yopmail.com";

        JSONObject linkRequestBody = new JSONObject("{\"mobile\":\"\",\"partnerId\":\"MMT\"}");
        JSONObject eligibilityRequestBody = new JSONObject("{\"accountId\":\"\",\"pan\":\"\",\"mobile\":\"\",\"email\":\"\",\"dob\":\"1988-06-01\",\"firstName\":\"Abhishek\",\"lastName\":\"Kumar\",\"gender\":\"MALE\",\"residenceType\":\"OWNED\",\"employmentType\":\"SALARIED\",\"monthlyIncome\":\"80001\",\"employerName\":\"Zest\",\"employedSince\":\"2020-03-11\",\"livingWith\":\"NO_ONE\",\"monthlyExpenses\":\"5000\",\"currentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Koramangal\",\"addressLine2\":\"Ramagondanahalli\"},\"permanentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Galaxy apartment\",\"addressLine2\":\"Koramangala\"}}");

        linkRequestBody.put("mobile", mobileNumber);

        eligibilityRequestBody.put("accountId", accountId);
        eligibilityRequestBody.put("pan", pan);
        eligibilityRequestBody.put("mobile", mobileNumber);
        eligibilityRequestBody.put("email", email);

        String access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Authorization", "Bearer " + access_token);
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("x-api-key","TdnSW3mdJ36E0pJZkvwIw9mOXlnzztUC2ZErXZHQ");
        requestHeaders.put("verificationid", "tripmoney");

//        Running Link API
        JSONObject linkAccountResponseJSON = new JSONObject((TripMoneyAction.linkAccount(accountId, linkRequestBody.toString(), requestHeaders)).getBody());
        Assert.assertEquals(linkAccountResponseJSON.getJSONObject("data").getJSONObject("account").getString("accountStatus"), "LINKED","Account status is not set to Linked");

//        Running eligibility api
        JSONObject eligibilityResponseJSON = new JSONObject((TripMoneyAction.eligibility(accountId,eligibilityRequestBody.toString(),requestHeaders).getBody()));
        Assert.assertEquals(eligibilityResponseJSON.getString("responseStatus"),"SUCCESS", "responseStatus is not set to SUCCESS");

        customerId = eligibilityResponseJSON.getJSONObject("data").getJSONObject("loan").getString("loanRefNum");

//        Clearing current R1 run data
        NonRiskDatabaseMiner.resetTripMoneyUserForRisk(mobileNumber);
        CibilDatabaseMiner.removeCLforTripMoney(customerId);

//        Setting up Cibil Data
        Thread.sleep(Constants.CIBIL_WAIT);
        CibilDatabaseMiner.clearExistingTransactionByCustomerId(customerId);
        CibilDatabaseMiner.mockCibilForTripMoney(customerId, pan, cibilV3score, mobileNumber);

//        Running TripMoney R1
        access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeadersStage1 = new HashMap<String, String>();
        requestHeadersStage1.put("Authorization", "Bearer " + access_token);
        requestHeadersStage1.put("Content-Type", "application/json");

        JSONArray results = new JSONArray(TripMoneyAction.tripMoneyStage1(customerId,merchantId, requestHeadersStage1).getBody());

        SoftAssert checks = new SoftAssert();

        for (int i =0; i< results.length(); i++){
            checks.assertTrue(results.getJSONObject(i).getBoolean("result"));
            checks.assertTrue(results.getJSONObject(i).getJSONObject("credit_limit").getInt("approved_amt")>=7000, "Approved_amt is not >= 7000");
        }


//        Preparing Rule executer api request body
        String ruleRequestBody = "{\"record\":{\"data\":{}},\"rules\":[{\"name\":\"TripMoneyCreditLimitGenerate\",\"params\":null,\"aggr\":[{\"label\":\"CIBILIncomeEstimationV2\",\"result\":true,\"final_states\":[],\"data\":{}},{\"label\":\"TripmoneyCIBILScoreCheck\",\"result\":true,\"status_code\":null,\"data\":{\"is_thin_customer\":false,\"cibil_score\":900,\"cibil_version\":\"V2\"}},{\"label\":\"FraudScorecardCheck\",\"result\":null,\"status_code\":\"ZMRSK921\",\"data\":{\"version\":null,\"zest_fraud_score\":null}},{\"label\":\"CIBILMobileUnenrichedCheck\",\"result\":true,\"status_code\":null,\"params\":null,\"data\":{\"mobile\":\"917935975582\",\"label\":\"in_unenriched\"}}]}]}";
        JSONObject ruleRequestBodyJSON = new JSONObject(ruleRequestBody);

//        extracting customer enrichment data and setting it into rule executer body
        JSONObject customerData = new JSONObject(RiskDatabaseMiner.getCustomerData(customerId));
        ruleRequestBodyJSON.getJSONObject("record").put("data", customerData);

        JSONObject cibil_income_estimation_v2 = customerData.getJSONObject("cibil").getJSONObject("cibil_income_estimation_v2");
        cibil_income_estimation_v2.put("net_monthly_income", cibilNetMonthlyIncome);

        JSONArray aggrRules = ruleRequestBodyJSON.getJSONArray("rules").getJSONObject(0).getJSONArray("aggr");

        boolean found = false;
        for(int i=0; i<aggrRules.length(); i++){
            if(aggrRules.getJSONObject(i).getString("label").equals("CIBILIncomeEstimationV2")){
                aggrRules.getJSONObject(i).put("data", cibil_income_estimation_v2);
                found = true;
                break;
            }
        }

        Assert.assertTrue(found, "cibil_income_estimation_v2 is missing in aggr array of TripMoneyCreditLimitGenerate Rule");

        JSONObject dag = new JSONObject(RiskDatabaseMiner.getCustomerJourneyDag(customerId, EngineType.TRIPMONEYSTAGE1.label));
        String journeyId = dag.getString("journey_id");

        String access_token_internal_services = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeadersRuleExecutor = new HashMap<String, String>();
        requestHeadersRuleExecutor.put("Authorization", "Bearer " + access_token_internal_services);
        requestHeadersRuleExecutor.put("Content-Type", "application/json");

        JSONObject ruleExecutorResponseBody = new JSONObject(RuleExecutorAction.runRuleEngine(journeyId, ruleRequestBodyJSON.toString(), requestHeadersRuleExecutor).getBody());
        JSONArray creditLimitLists = ruleExecutorResponseBody.getJSONArray("responses").getJSONObject(0).getJSONObject("data").getJSONArray("credit_limit");

        for (int i =0; i< creditLimitLists.length(); i++)
            checks.assertEquals(creditLimitLists.getJSONObject(i).getBoolean("result"), expectedResult);

        checks.assertAll();
    }

    @Test(description = "Trip Money End to End", groups = {Group.TRIP_MONEY_END_TO_END}, dataProvider="TRIP_MONEY_STAGE1", dataProviderClass = TripMoneyDataprovider.class, enabled = false)
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verifying User is able to run trip money end to end flow")
    public void verify_trip_money_End_To_End(String testCaseName, int cibilV3score, boolean expectedResult) throws Exception {

        String merchantId = "3c528762-7485-4d93-bd14-488bc14596b4";
        String customerId;
        String mobileNumber = CommonUtility.randomMobileNumber();
        String pan = CommonUtility.randomPAN();
        String accountId = pan;
        String email = mobileNumber + "@yopmail.com";

        JSONObject linkRequestBody = new JSONObject("{\"mobile\":\"\",\"partnerId\":\"MMT\"}");
        JSONObject eligibilityRequestBody = new JSONObject("{\"accountId\":\"\",\"pan\":\"\",\"mobile\":\"\",\"email\":\"\",\"dob\":\"1988-06-01\",\"firstName\":\"Abhishek\",\"lastName\":\"Kumar\",\"gender\":\"MALE\",\"residenceType\":\"OWNED\",\"employmentType\":\"SALARIED\",\"monthlyIncome\":\"80001\",\"employerName\":\"Zest\",\"employedSince\":\"2020-03-11\",\"livingWith\":\"NO_ONE\",\"monthlyExpenses\":\"5000\",\"currentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Koramangal\",\"addressLine2\":\"Ramagondanahalli\"},\"permanentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Galaxy apartment\",\"addressLine2\":\"Koramangala\"}}");

        linkRequestBody.put("mobile", mobileNumber);

        eligibilityRequestBody.put("accountId", accountId);
        eligibilityRequestBody.put("pan", pan);
        eligibilityRequestBody.put("mobile", mobileNumber);
        eligibilityRequestBody.put("email", email);

        String access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Authorization", "Bearer " + access_token);
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("x-api-key","TdnSW3mdJ36E0pJZkvwIw9mOXlnzztUC2ZErXZHQ");
        requestHeaders.put("verificationid", "tripmoney");

//        Running Link API
        JSONObject linkAccountResponseJSON = new JSONObject((TripMoneyAction.linkAccount(accountId, linkRequestBody.toString(), requestHeaders)).getBody());
        Assert.assertEquals(linkAccountResponseJSON.getJSONObject("data").getJSONObject("account").getString("accountStatus"), "LINKED","Account status is not set to Linked");

//        Running eligibility api
        JSONObject eligibilityResponseJSON = new JSONObject((TripMoneyAction.eligibility(accountId,eligibilityRequestBody.toString(),requestHeaders).getBody()));
        Assert.assertEquals(eligibilityResponseJSON.getString("responseStatus"),"SUCCESS", "responseStatus is not set to SUCCESS");

        customerId = eligibilityResponseJSON.getJSONObject("data").getJSONObject("loan").getString("loanRefNum");

//        Clearing current R1 run data
        Thread.sleep(Constants.TRIPMONEY_WAIT);
        NonRiskDatabaseMiner.resetTripMoneyUserForRisk(mobileNumber);
        CibilDatabaseMiner.removeCLforTripMoney(customerId);

//        Setting up Cibil Data
        Thread.sleep(Constants.CIBIL_WAIT);
        CibilDatabaseMiner.clearExistingTransactionByCustomerId(customerId);
        CibilDatabaseMiner.mockCibilForTripMoney(customerId, pan, cibilV3score, mobileNumber);

//        Running TripMoney R1
        access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeadersStage1 = new HashMap<String, String>();
        requestHeadersStage1.put("Authorization", "Bearer " + access_token);
        requestHeadersStage1.put("Content-Type", "application/json");

        JSONArray results = new JSONArray(TripMoneyAction.tripMoneyStage1(customerId,merchantId, requestHeadersStage1).getBody());

        SoftAssert checks = new SoftAssert();

        for (int i =0; i< results.length(); i++){
            checks.assertEquals(results.getJSONObject(i).getBoolean("result"), expectedResult);
            if (expectedResult)
                checks.assertTrue(results.getJSONObject(i).getJSONObject("credit_limit").getInt("approved_amt")>=7000, "Approved_amt is not >= 7000");
        }

//        Verifying CLR1, CLR2, CLR3, Affordable amount calculation
        JSONObject dag = new JSONObject(RiskDatabaseMiner.getCustomerJourneyDag(customerId, EngineType.TRIPMONEYSTAGE1.label));

        JSONObject tripMoneyCreditLimitCheckData = dag.getJSONObject("rules").getJSONObject("TripMoneyCreditLimitCheck").getJSONObject("data");

        int MIN_TERM_IN_MONTHS = tripMoneyCreditLimitCheckData.getInt("min_term_in_months");
        double RATIO_TOTAL_EMI_TO_NMI = tripMoneyCreditLimitCheckData.getDouble("ratio_total_emi_to_nmi");
        double RATIO_ZEST_EMI_TO_NMI = tripMoneyCreditLimitCheckData.getDouble("ratio_zest_emi_to_nmi");
        double MAX_INTEREST_RATE = tripMoneyCreditLimitCheckData.getDouble("max_interest_rate");
        double perMonthEMI = tripMoneyCreditLimitCheckData.getDouble("per_month_emi");
        double monthlyIncome = tripMoneyCreditLimitCheckData.getDouble("CIBILIncomeEstimationCheck");

        double totalMonthlySpending = eligibilityRequestBody.getDouble("monthlyExpenses");

        double actualClr1 = tripMoneyCreditLimitCheckData.getDouble("clR1");
        double actualClr2 = tripMoneyCreditLimitCheckData.getDouble("clR2");
        double actualClr3 = tripMoneyCreditLimitCheckData.getDouble("clR3");

        double actualInterest = tripMoneyCreditLimitCheckData.getDouble("interest");
        double actualAffordableAmt = tripMoneyCreditLimitCheckData.getDouble("affordable_amt");

        double expectedInterest = MIN_TERM_IN_MONTHS / (1.0 + MAX_INTEREST_RATE * MIN_TERM_IN_MONTHS);
        double clR1 = RATIO_ZEST_EMI_TO_NMI * monthlyIncome * expectedInterest;
        double clR2 = ((monthlyIncome - totalMonthlySpending - perMonthEMI) * expectedInterest);
        double clR3 = ((RATIO_TOTAL_EMI_TO_NMI * monthlyIncome) - perMonthEMI) * expectedInterest;
        double affordableAmount = Math.min(clR1, Math.min(clR2, clR3));

//        rounding to 2 decimal places
        double scale = Math.pow(10, 2);
        affordableAmount = Math.round(affordableAmount * scale) / scale;

        checks.assertEquals(actualClr1, clR1);
        checks.assertEquals(actualClr2, clR2);
        checks.assertEquals(actualClr3, clR3);

        checks.assertEquals(actualAffordableAmt, affordableAmount);
        checks.assertEquals(actualInterest, expectedInterest);

//        Verifying enrichment
        JSONObject customerData = new JSONObject(RiskDatabaseMiner.getCustomerData(customerId));

        if(cibilV3score==-1) {
            JSONObject l0_score = customerData.getJSONObject("score").getJSONObject("l0_score");

            int zest_l0_score = Integer.parseInt(l0_score.getString("zest_l0_score"));
            int version = Integer.parseInt(l0_score.getString("version"));
            boolean is_thin_customer = l0_score.getBoolean("is_thin_customer");
            int cibil_score = Integer.parseInt(l0_score.getString("cibil_score"));
            String cust_type = l0_score.getString("cust_type");

            checks.assertEquals(cust_type, "NTC");
            checks.assertEquals(cibil_score, -1);
            checks.assertEquals(is_thin_customer, true);
            checks.assertEquals(version, 5);
            checks.assertTrue(zest_l0_score > 0);
        } else{
            JSONObject l0_thick_v5 = customerData.getJSONObject("score").getJSONObject("l0_thick_v5");

            int zest_l0v5_score = Integer.parseInt(l0_thick_v5.getString("zest_l0v5_score"));
            int version = Integer.parseInt(l0_thick_v5.getString("version"));
            boolean is_thin_customer = l0_thick_v5.getBoolean("is_thin_customer");
            int cibil_score = Integer.parseInt(l0_thick_v5.getString("cibil_score"));

            checks.assertEquals(cibil_score, cibilV3score);
            checks.assertEquals(is_thin_customer, false);
            checks.assertEquals(version, 5);
            checks.assertTrue(zest_l0v5_score > 0);
        }

        requestHeaders.put("Content-Type","multipart/form-data");
        requestHeaders.remove("Authorization");

        JSONObject responseBody = new JSONObject(TripMoneyAction.docUpload(accountId, requestHeaders));

        checks.assertEquals(responseBody.getString("responseStatus"), "SUCCESS");
        checks.assertEquals(responseBody.getJSONObject("data").getJSONObject("loan").getJSONObject("docUpload").getString("status"), "SUCCESS");

        checks.assertAll();
    }

    @Test(description = "Create Trip Money Stage 1 approved user", groups = {Group.CREATE_TRIP_MONEY_R1_APPROVED})
    @Severity(SeverityLevel.BLOCKER)
    @Description("Create Trip Money Stage 1 approved user")
    public void create_trip_money_stage1_appoved_customer() throws Exception {

        String merchantId = "3c528762-7485-4d93-bd14-488bc14596b4";
        String customerId;
        String mobileNumber = CommonUtility.randomMobileNumber();
        String pan = CommonUtility.randomPAN();
        String accountId = pan;
        String email = mobileNumber + "@yopmail.com";
        int cibilV3score = 799;

        JSONObject linkRequestBody = new JSONObject("{\"mobile\":\"\",\"partnerId\":\"MMT\"}");
        JSONObject eligibilityRequestBody = new JSONObject("{\"accountId\":\"\",\"pan\":\"\",\"mobile\":\"\",\"email\":\"\",\"dob\":\"1988-06-01\",\"firstName\":\"Abhishek\",\"lastName\":\"Kumar\",\"gender\":\"MALE\",\"residenceType\":\"OWNED\",\"employmentType\":\"SALARIED\",\"monthlyIncome\":\"80001\",\"employerName\":\"Zest\",\"employedSince\":\"2020-03-11\",\"livingWith\":\"NO_ONE\",\"monthlyExpenses\":\"5000\",\"currentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Koramangal\",\"addressLine2\":\"Ramagondanahalli\"},\"permanentAddress\":{\"pinCode\":\"560066\",\"addressLine1\":\"Galaxy apartment\",\"addressLine2\":\"Koramangala\"}}");

        linkRequestBody.put("mobile", mobileNumber);

        eligibilityRequestBody.put("accountId", accountId);
        eligibilityRequestBody.put("pan", pan);
        eligibilityRequestBody.put("mobile", mobileNumber);
        eligibilityRequestBody.put("email", email);

        String access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Authorization", "Bearer " + access_token);
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("x-api-key","TdnSW3mdJ36E0pJZkvwIw9mOXlnzztUC2ZErXZHQ");
        requestHeaders.put("verificationid", "tripmoney");

//        Running Link API
        JSONObject linkAccountResponseJSON = new JSONObject((TripMoneyAction.linkAccount(accountId, linkRequestBody.toString(), requestHeaders)).getBody());
        Assert.assertEquals(linkAccountResponseJSON.getJSONObject("data").getJSONObject("account").getString("accountStatus"), "LINKED","Account status is not set to Linked");

//        Running eligibility api
        JSONObject eligibilityResponseJSON = new JSONObject((TripMoneyAction.eligibility(accountId,eligibilityRequestBody.toString(),requestHeaders).getBody()));
        Assert.assertEquals(eligibilityResponseJSON.getString("responseStatus"),"SUCCESS", "responseStatus is not set to SUCCESS");

        customerId = eligibilityResponseJSON.getJSONObject("data").getJSONObject("loan").getString("loanRefNum");

//        Clearing current R1 run data
        Thread.sleep(Constants.TRIPMONEY_WAIT);
        NonRiskDatabaseMiner.resetTripMoneyUserForRisk(mobileNumber);
        CibilDatabaseMiner.removeCLforTripMoney(customerId);

//        Setting up Cibil Data
        Thread.sleep(Constants.CIBIL_WAIT);
        CibilDatabaseMiner.clearExistingTransactionByCustomerId(customerId);
        CibilDatabaseMiner.mockCibilForTripMoney(customerId, pan, cibilV3score, mobileNumber);

//        Running TripMoney R1
        access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeadersStage1 = new HashMap<String, String>();
        requestHeadersStage1.put("Authorization", "Bearer " + access_token);
        requestHeadersStage1.put("Content-Type", "application/json");

        JSONArray results = new JSONArray(TripMoneyAction.tripMoneyStage1(customerId,merchantId, requestHeadersStage1).getBody());

        SoftAssert checks = new SoftAssert();

        for (int i =0; i< results.length(); i++){
            checks.assertTrue(results.getJSONObject(i).getBoolean("result"));
            checks.assertTrue(results.getJSONObject(i).getJSONObject("credit_limit").getInt("approved_amt")>=7000, "Approved_amt is not >= 7000");
        }

        checks.assertAll();
    }

}
