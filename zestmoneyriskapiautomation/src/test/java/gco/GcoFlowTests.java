package test.java.gco;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import main.java.utils.listners.Logger;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import main.java.authService.utility.AuthUtility;
import main.java.dagManagerService.merchant.actions.GcoFLowAction;
import main.java.databaseHelper.CibilDatabaseMiner;
import main.java.databaseHelper.NonRiskDatabaseMiner;
import main.java.enums.TokenScopeType;
import main.java.gco.actions.MerchantFlowAction;
import main.java.interfaces.Group;
import main.java.utils.CommonFunctions;
import main.java.utils.CommonUtility;
import test.java.Commons; 

public class GcoFlowTests extends Commons {

	@Test(description = "Merchant flow",groups = {Group.MERCHANT_FLOW})
	@Severity(SeverityLevel.BLOCKER)
	@Description("Testing Merchant flow")
	public void verifyGcMerchantFlow() throws Exception{
		//String merchantId = "d1131025-65ba-40ed-90c3-92841e06314b";
		//String walletId="flipkartgiftcard";
		String merchantId="d6eb9c55-6690-4525-987c-655a1be59083";
		String walletId="amazongiftcard";

		String customerId;
		String applicationId;
		String mobileNumber = CommonUtility.randomMobileNumber();
		String pan = CommonUtility.randomPAN();
		String BankNumber = CommonUtility.randomdigitPhone(20);
		String email = mobileNumber + "@yopmail.com";
		String otp;
		int amount = 10000;


		String access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

		//Preparing the common HTTP header information
		Map<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");

		//Create application
		JSONObject loanApplicationDetailsRequestBody = new JSONObject("{\"walletId\":\"\",\"reason\":\"Product\",\"amount\":10000,\"customerId\":null,\"mobileNumber\":\"\",\"numberOfMonths\":3}");
		loanApplicationDetailsRequestBody.put("mobileNumber",mobileNumber );
		loanApplicationDetailsRequestBody.put("walletId",walletId );

		JSONObject loanApplicationDetailsResponseJSONBody = new JSONObject((MerchantFlowAction.postLoanApplicationDetails(loanApplicationDetailsRequestBody.toString(),requestHeaders)).getBody());
		applicationId = loanApplicationDetailsResponseJSONBody.getString("applicationId");

		//Fetching payment gatewayid
		String paymentGatewayId1=loanApplicationDetailsResponseJSONBody.getString("applicationUrl");
		String paymentGatewayId=CommonFunctions.getQueryMap(paymentGatewayId1).get("paymentGatewayId");

		//Get Available emi options
		JSONObject availableEmiPlansForProductResponseJSON = new JSONObject((MerchantFlowAction.getAvailableEmiPlansForProduct(merchantId, amount, paymentGatewayId)));
		Assert.assertEquals(availableEmiPlansForProductResponseJSON.getInt("statusCode"), 200, "Statuscode is other than 200");

		//Get loan application details
		JSONObject ApplicationDetailsResponseJSON = new JSONObject((MerchantFlowAction.getLoanApplicationDetails(applicationId)));
		Assert.assertEquals(ApplicationDetailsResponseJSON.getInt("statusCode"), 200, "Statuscode is other than 200");

		//customer details
		JSONObject customerDetailsRequestBody = new JSONObject("{\"MerchantCustomerId\":null,\"ApplicationId\":\"\"}");
		customerDetailsRequestBody.put("ApplicationId", applicationId);

		JSONObject customerDetailsResponseJSON = new JSONObject((MerchantFlowAction.postCustomerDetails(customerDetailsRequestBody.toString(), requestHeaders)));
		Assert.assertEquals(customerDetailsResponseJSON.getInt("statusCode"), 200, "Statuscode is other than 200");

		//From here onwords same for instore and gco flow
		//Get OTP ID
		JSONObject mobileOtpRequestBody = new JSONObject("{\"MobileNumber\":\"\",\"MessageParams\":{\"MerchantId\":\"\"}}");
		mobileOtpRequestBody.put("MobileNumber", "91"+mobileNumber);
		mobileOtpRequestBody.put("MerchantId", merchantId);

		JSONObject postDetailsToGetOtpIdResponseJSON = new JSONObject((MerchantFlowAction.postDetailsToGetOtpId(mobileOtpRequestBody.toString(), requestHeaders)).getBody());
		String otpId=postDetailsToGetOtpIdResponseJSON.getString("OtpId").toString();

		//Get OTP
		otp=NonRiskDatabaseMiner.findOtpByMobileNumber(mobileNumber, "Otp");
		
		//Preparing common url encoded header
		Map<String, String> requestUrlEncHeader = new HashMap<String, String>();
		requestUrlEncHeader.put("Content-Type", "application/x-www-form-urlencoded");
		requestUrlEncHeader.put("token", "token:135b043150475d7288b936606e086af5c1fb7f0eff15256748f5ad078ebdcf85");

		String acr_values="isOtp:true&isTruecaller:false&mobile:91"+mobileNumber+"&email:"+email+"&loginContext:2&otpId:"+otpId+"&version:2";
		List<NameValuePair> postEmailIdToSignUpWithTokenRequestBody=new ArrayList<>();
		postEmailIdToSignUpWithTokenRequestBody.add(new BasicNameValuePair("grant_type","password"));
		postEmailIdToSignUpWithTokenRequestBody.add(new BasicNameValuePair("scope","my_accounts user_journey openid"));
		postEmailIdToSignUpWithTokenRequestBody.add(new BasicNameValuePair("username", "91"+mobileNumber));
		postEmailIdToSignUpWithTokenRequestBody.add(new BasicNameValuePair("password", otp));
		postEmailIdToSignUpWithTokenRequestBody.add(new BasicNameValuePair("acr_values", acr_values));
		postEmailIdToSignUpWithTokenRequestBody.add(new BasicNameValuePair("client_id", "9add8006-f45a-11e7-8c3f-9a214cf093ae"));
		postEmailIdToSignUpWithTokenRequestBody.add(new BasicNameValuePair("client_secret", "testPassword"));

		JSONObject postEmailIdToSignUpWithTokenResponseJSON = new JSONObject((MerchantFlowAction.postEmailIdToSignUpWithToken(postEmailIdToSignUpWithTokenRequestBody, requestUrlEncHeader)));
		Assert.assertEquals(postEmailIdToSignUpWithTokenResponseJSON.getInt("statusCode"), 200, "Statuscode is other than 200");

		requestHeaders.put("Authorization", "Bearer " + access_token);

		//Getting customerId by mobile number
		customerId = NonRiskDatabaseMiner.findCustomerIdByMobileNumber(mobileNumber);

		//Assigning application to customer
		JSONObject applicationAssignedToCustomerResponceJSON =new JSONObject((MerchantFlowAction.postApplicationFlowLoanApplicationFlowAfterCustomerCreation(customerId, applicationId,"",requestHeaders)));
		Assert.assertEquals(applicationAssignedToCustomerResponceJSON.getInt("statusCode"), 200, "Statuscode is other than 200");
		
		//PD and ED details submission
		CommonFunctions.pDAndEdSubmission(customerId, pan, requestHeaders);
		
		//PreAcceptV2
		JSONObject preAcceptV2ResponceBody = new JSONObject(GcoFLowAction.preAcceptV2(customerId, applicationId, requestHeaders).getBody());
		if(preAcceptV2ResponceBody.getInt("decision_code")==3) {
			Logger.log("The application is rejected at PreAcceptV2");
			return;
		}

		//Mocking netbanking Perfios
		CibilDatabaseMiner.mockNetBankingPerfiosData(customerId, mobileNumber);

		//LoanAgreement
		JSONObject loanAgrrementRequestBody = new JSONObject("{\"customerId\":\"\",\"applicationId\":\"\",\"day\":5}");
		loanAgrrementRequestBody.put("customerId", customerId);
		loanAgrrementRequestBody.put("applicationId", applicationId);
		JSONObject loanAgreementResponceJSON = new JSONObject(MerchantFlowAction.acceptLoanAgreement(loanAgrrementRequestBody.toString(), requestHeaders));
		Assert.assertEquals(loanAgreementResponceJSON.getInt("statusCode"), 201, "Statuscode is other than 201");

		//Update Loan agreement status
		JSONObject loanAgreementStatusRequestBody = new JSONObject("{\"Status\":\"LoanAgreementAccepted\",\"CancellationReason\":null,\"OtherReason\":null,\"OtpId\":null}");
		JSONObject loanAgreementAcceptedStatusResponceJSON=new JSONObject(MerchantFlowAction.updateLoanAgreementAccepted(applicationId, loanAgreementStatusRequestBody.toString(), requestHeaders));
		Assert.assertEquals(loanAgreementAcceptedStatusResponceJSON.getInt("statusCode"), 200, "Statuscode is other than 200");

		//Post Bank details
		JSONObject postBankDetailsRequestBody = new JSONObject("{\"CustomerId\":\"\",\"AccountHoldersName\": \"Maruti HIrave\",\"AccountNumber\": \"\",\"AccountType\": \"Savings\",\"BankId\": \"UTIB\",\"BankName\": \"Axis Bank\",\"CreatedOn\": \"\",\"CustomBankName\": null,\"IFSC\": \"UTIB0000009\",\"Source\": \"UserFilled\",\"TrustedMerchantId\": null,\"IsAadhaarLinkedToBankAccount\": null,\"ApplicationId\": null}");
		postBankDetailsRequestBody.put("CustomerId", customerId);
		postBankDetailsRequestBody.put("AccountNumber",BankNumber);
		JSONObject postBankDetailsResponceJSON = new JSONObject(MerchantFlowAction.postBankDetails(customerId,postBankDetailsRequestBody.toString(), requestHeaders));
		Assert.assertEquals(postBankDetailsResponceJSON.getInt("statusCode"), 201, "Statuscode is other than 201");

		//PostAcceptV2
		JSONObject postAcceptV2ResponceBody = new JSONObject(GcoFLowAction.postAcceptV2(customerId, applicationId, requestHeaders).getBody());
		if(postAcceptV2ResponceBody.getInt("decision_code")==3) {
			Logger.log("The application is rejected at PostAcceptV2");
			return;
		}
		
		//KYC and Mandate Flow
		CommonFunctions.KycMandateFunction(customerId, requestHeaders);

	}

}
