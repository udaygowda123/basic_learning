package test.java.ruleExecutorService;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import main.java.authService.utility.AuthUtility;
import main.java.core.Response;
import main.java.enrichmentService.actions.EnrichmentAction;
import main.java.enrichmentService.dtos.EnrichmentRequest;
import main.java.enums.TokenScopeType;
import main.java.interfaces.Group;
import main.java.ruleExecuterService.actions.RuleExecutorAction;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import test.java.Commons;

import java.io.IOException;
import java.util.*;

public class RuleExecutorTests extends Commons {

    @Test(description = "Verifying if enrichment is correctly done", groups = {Group.RULE_EXECUTOR})
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verifying if enrichment is correctly done")
    public void verifyEnrichment() throws IOException {

        String access_token = AuthUtility.getAccessToken(TokenScopeType.INTERNAL_SERVICES);

//        Preparing the common HTTP header information
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Authorization", "Bearer " + access_token);
        requestHeaders.put("Content-Type", "application/json");

        EnrichmentRequest enrichmentRequest = new EnrichmentRequest();

        enrichmentRequest.setJourneyId("3e56b967-d551-4861-b957-f6ff46fde973");
        enrichmentRequest.setJourneyType("PreApproveV2Stage1");
        enrichmentRequest.setDataVersion(null);
        enrichmentRequest.setMerchantId("4369bab6-0f87-4d9b-85df-10718781fd24");
        enrichmentRequest.setCustomerId("8f1acc3e-d48e-4f64-902e-1ca4c64fd8e0");

        ArrayList<String> enrichers = new ArrayList<String>(
                Arrays.asList("AgeEnricher","AadharDetailsEnricher","GeoIPCountryEnrichment","SocialDetailsEnricher","EntityAccessControlEnrichment","L0ScoreEnrichment","PhoneDetailsEnricher","CibilIncomeEstimationV2Enricher","CibilEmiEstimation","EmploymentAndObligationsEnricher","L0ScoreEnrichment","LoanDetailsEnricher","EmploymentAndObligationsEnricher","NewCustomerEnricher","PhoneDetailsEnricher","EmailDetailsEnricher","AddressesEnricher","SocialDetailsEnricher","PANDetailsEnricher","L0ScoreEnrichment","PerfiosDetailsEnricher","CovidDataEnricher","EmploymentAndObligationsEnricher","FraudScorecardV4Enricher","CibilEmiEstimation","CibilIncomeEstimationV2Enricher","EmploymentAndObligationsEnricher","L0ScoreEnrichment","EmailDetailsEnricher","L0ScoreEnrichment","PANDetailsEnricher","L0ScoreEnrichment","AddressesEnricher","LoanDetailsEnricher","CibilIncomeEstimationV2Enricher","EmploymentAndObligationsEnricher","L0ScoreEnrichment","L0ThickV5Enrichment","MerchantIdEnricher")
        );

        enrichmentRequest.setNextEnrichmentSet(enrichers);

        Response enrichmentResponse = EnrichmentAction.enrich((new ObjectMapper().writeValueAsString(enrichmentRequest)), requestHeaders);

        JSONObject record = new JSONObject(enrichmentResponse.getBody()).getJSONObject("record");

        Assert.assertEquals(record.getString("journey_id"), enrichmentRequest.getJourneyId(), "journey_id do not match");

//        preparing for RuleEngineRun

        String sampleRequestBody = "{\"record\":{},\"rules\":[{\"name\":\"L0V5ScoreCardCheck\",\"params\":{},\"aggr\":[]}]}";

        JSONObject ruleExecutorRequestBody = new JSONObject(sampleRequestBody);
        ruleExecutorRequestBody.put("record", record);

        Response ruleExecutorResponse = RuleExecutorAction.runRuleEngine(enrichmentRequest.getJourneyId(), ruleExecutorRequestBody.toString(), requestHeaders);

//        Write the assertions here

    }

}
