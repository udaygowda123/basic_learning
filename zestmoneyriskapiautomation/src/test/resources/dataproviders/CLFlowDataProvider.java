package test.resources.dataproviders;

import main.java.constants.RiskConstants;
import main.java.utils.CommonFunctions;
import main.java.utils.CommonUtility;
import org.testng.annotations.DataProvider;

public class CLFlowDataProvider {

    @DataProvider(name = "STAGE1_SALARIED_CHECK", parallel = true)
    public static Object[][] get_customer_declared_income_TestCases() throws Exception {
        String[] merchantIds = {RiskConstants.ZEST_MERCHANT_ID, RiskConstants.PINESTORE_DP, RiskConstants.PINELAB_MERCHANTS};
        String[] merchant = {"ZEST_MERCHANT_ID", "PINESTORE_DP", "PINELAB_MERCHANTS"};
        Object[][] obj = new Object[merchant.length * 3][4];
        String checkpointName = "SalariedCheck";
        String param = "min_income";
        int fallbackValue = 11000;
        for (int k = 0; k < merchant.length; k++) {
            String[] queryParams;
            if (merchant[k].equalsIgnoreCase("APPLETVS_MERCHANTS") || merchant[k].equalsIgnoreCase("PINELAB_MERCHANTS")) {
                queryParams = new String[]{merchant[k], "PreApproveV2Stage1", "others"};
            } else {
                queryParams = new String[]{merchant[k], "PreApproveV2Stage1", "new_customer"};
            }
            int salary = CommonFunctions.getCutOffFromDag(queryParams, checkpointName, param, fallbackValue);

            int start = k * 3;
            obj[start + 0] = new Object[]{merchantIds[k], checkpointName + " check for : " + merchant[k] + " : " + (salary - 1), (salary - 1), "3"};
            obj[start + 1] = new Object[]{merchantIds[k], checkpointName + " check for : " + merchant[k] + " : " + (salary), (salary), "2"};
            obj[start + 2] = new Object[]{merchantIds[k], checkpointName + " check for : " + merchant[k] + " : " + (salary + 1), (salary + 1), "2"};
        }
        return obj;
    }


    @DataProvider(name = "STAGE1_MIN_MAX_AGE_CHECK", parallel = true)
    public static Object[][] get_customer_min_and_max_age_TestCases() throws Exception {
        String[] merchantIds = {RiskConstants.ZEST_MERCHANT_ID, RiskConstants.PINESTORE_DP, RiskConstants.PINELAB_MERCHANTS};
        String[] merchant = {"ZEST_MERCHANT_ID", "PINESTORE_DP", "PINELAB_MERCHANTS"};

        Object[][] obj = new Object[merchant.length * 6][5];
        String checkpointName;
        String param = "min_age";
        int fallbackValue = 18;
        String maxAgeCheckpointName = "MaxAgeCheck";
        String maxAgeParam = "max_age";
        int maxAgeFallbackValue = 65;

        for (int k = 0; k < merchant.length; k++) {
            checkpointName = "MinAgeCheck";
            String[] queryParams;
            if (merchant[k].equalsIgnoreCase("APPLETVS_MERCHANTS") || merchant[k].equalsIgnoreCase("PINELAB_MERCHANTS")) {
                queryParams = new String[]{merchant[k], "PreApproveV2Stage1", "others"};
            } else {
                queryParams = new String[]{merchant[k], "PreApproveV2Stage1", "new_customer"};
            }
            int min_age = CommonFunctions.getCutOffFromDag(queryParams, checkpointName, param, fallbackValue);
            int max_age = CommonFunctions.getCutOffFromDag(queryParams, maxAgeCheckpointName, maxAgeParam, maxAgeFallbackValue);

            int start = k * 6;
            obj[start + 0] = new Object[]{merchantIds[k], checkpointName + " check for : " + merchant[k] + " : " + (min_age - 1), false, CommonUtility.findDOBByAge(min_age - 1), false};
            obj[start + 1] = new Object[]{merchantIds[k], checkpointName + " check for : " + merchant[k] + " : " + (min_age), false, CommonUtility.findDOBByAge(min_age), true};
            obj[start + 2] = new Object[]{merchantIds[k], checkpointName + " check for : " + merchant[k] + " : " + (min_age + 1), false, CommonUtility.findDOBByAge(min_age + 1), true};
            obj[start + 3] = new Object[]{merchantIds[k], checkpointName + " check for : " + merchant[k] + " : " + (max_age - 1), true, CommonUtility.findDOBByAge(max_age - 1), true};
            obj[start + 4] = new Object[]{merchantIds[k], checkpointName + " check for : " + merchant[k] + " : " + (max_age), true, CommonUtility.findDOBByAge(max_age), true};
            obj[start + 5] = new Object[]{merchantIds[k], checkpointName + " check for : " + merchant[k] + " : " + (max_age + 1), true, CommonUtility.findDOBByAge(max_age + 1), false};
        }
        return obj;
    }
}

