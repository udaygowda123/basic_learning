package test.resources.dataproviders;

import main.java.utils.CommonUtility;
import org.testng.annotations.DataProvider;

public class TripMoneyDataprovider {

    @DataProvider(name = "TRIP_MONEY_STAGE1", parallel = true)
    public static Object[][] get_customer_type_TestCases() {

        return new Object[][]{
                {"Thick Customer", 574, false},
                {"Thick Customer", 575, true},
                {"Thick Customer", 576, true},
                {"Thin Customer", -1, true}
        };
    }

    @DataProvider(name = "TRIP_MONEY_STAGE1_L0ThickV5_L0V5", parallel = true)
    public static Object[][] get_L0ThickV5_TestCases() {

        int l0ThickV5 = 459;
        int loV5 = 566;

        return new Object[][]{
                {"l0ThickV5: "+(l0ThickV5-1), false, (l0ThickV5-1)+"", 849, false},
                {"l0ThickV5: "+(l0ThickV5), false, l0ThickV5+"", 849, true},
                {"l0ThickV5: "+(l0ThickV5+1), false, (l0ThickV5+1)+"", 849, true},
                {"l0V5 thin customer: "+(loV5-1), true, (loV5-1)+"", -1, false},
                {"l0V5 thin customer: "+(loV5), true, loV5+"", -1, true},
                {"l0V5 thin customer: "+(loV5+1), true, (loV5+1)+"", -1, true}
        };
    }

    @DataProvider(name = "TRIP_MONEY_STAGE1_CibilNetMonthlyIncome", parallel = true)
    public static Object[][] get_CibilNetMonthlyIncome_TestCases() {

        double income = 20000;

        return new Object[][]{
                {"CIBIL Monthly Income: "+(income-1), (income-1), 849, false},
                {"CIBIL Monthly Income: "+(income), income, 849, true},
                {"CIBIL Monthly Income: "+(income+1), (income+1), 849, true}
        };
    }

    @DataProvider(name = "TRIP_MONEY_STAGE1_AGE_CHECK", parallel = true)
    public static Object[][] get_customer_age_TestCases() {

        int min_age = 21;
        int max_age = 63;

        return new Object[][]{
                {"Age: "+(min_age-1), false, CommonUtility.findDOBByAge(min_age-1), false},
                {"Age: "+(min_age), false, CommonUtility.findDOBByAge(min_age), true},
                {"Age: "+(min_age+1), false, CommonUtility.findDOBByAge(min_age+1), true},
                {"Age: "+(max_age+1), true, CommonUtility.findDOBByAge(max_age+1), false},
                {"Age: "+(max_age), true, CommonUtility.findDOBByAge(max_age), true},
                {"Age: "+(max_age-1), true, CommonUtility.findDOBByAge(max_age-1), true}
        };
    }

    @DataProvider(name = "TRIP_MONEY_STAGE1_SALARIED_CHECK", parallel = true)
    public static Object[][] get_customer_declared_income_TestCases() {

        int salary = 20000;

        return new Object[][]{
                {"Salary: "+(salary-1), (salary-1), false},
                {"Salary: "+(salary), (salary), true},
                {"Salary: "+(salary+1), (salary+1), true}
        };
    }

}