package test.resources.dataproviders;

import org.testng.annotations.DataProvider;

public class EngineTypeDataprovider {

    @DataProvider(name = "ENGINE_TYPE")
    public static Object[][] get_engine_type_TestCases() {

        return new Object[][]{
//                {"Engine type: CheckoutV2", "CheckoutV2"},
                {"Engine type: PreApproveV2Stage2", "PreApproveV2Stage2"},
                {"Engine type: PreApproveV2Stage1", "PreApproveV2Stage1"},
                {"Engine type: PreAcceptV2", "PreAcceptV2"},
                {"Engine type: PostAcceptV2", "PostAcceptV2"},
                {"Engine type: TripMoneyStage1", "TripMoneyStage1"},
                {"Engine type: TripMoneyStage2", "TripMoneyStage2"}
        };
    }

}